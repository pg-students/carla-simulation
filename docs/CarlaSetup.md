# Carla Setup on Windows

### Basic info
Pracujemy na wersji 0.9.5, ponieważ jest to najnowsza wersja, która wspiera system operacyjny
Windows, niezależnie od systemu nasze wersje muszą się zgadzać. <br/>
W związku z powyższym będziemy pracowali na Pythonie 3.7.x(najlepiej wersja stabilna, jedna przed
ostatnim releasem). Przejrzałem resztę buildów i niestety wszystkie wydane pod Windows są oparte o 3.7.
Aby uniknąć workaround'ów nie bedziemy przechodzić na wersję 3.6 i każdemu z grupy Python API będzie się zgadzało.

### Wymagania
* #### Sprzętowe
    * Quad-core Intel or AMD processor, 2.5 GHz or faster
    * NVIDIA GeForce 470 GTX or AMD Radeon 6870 HD series card or higher
    * 8 GB RAM
    * 10GB of hard drive space for the simulator setup
* #### Softwarowe
    * Unreal Engine(4.22.x) (https://www.unrealengine.com/en-US/download) - pobranie samego 
    Epic Games Launchera nie wystarcza
    * Python 3.7 enviroment
    
### Setup
* Pobieramy wersję 0.9.5 https://github.com/carla-simulator/carla/blob/master/Docs/download.md
* Tworzymy conda enviroment/virtualenv z pythonem 3.7.x np.: <br/>
    ``conda create -n carla python=3.7``
* Instalacja pygame i numpy: <br/>
    ``conda activate carla`` <br/>
    ``conda install pygame numpy`` <br/>
* Po rozpakowaniu gotowego buildu mozemy uruchomic server za pomocą <br/>
``CarlaUE4.exe``
* Teraz do uruchomionego servera mozemy dodac trochę dynamiki(server musi byc uruchomiony): <br/>
Wchodzimy do folderu ./PythonAPI/examples: <br/>
    * ``pip install -r requirements.txt `` <br/>
    * ``python spawn_npc.py -n 50`` - do mapy zostanie dodanych 50 pojazdów <br/>
    * ``python dynamic_weather.py`` - do mapy zostanie dodane slonce, pozniej deszcz <br/>


    