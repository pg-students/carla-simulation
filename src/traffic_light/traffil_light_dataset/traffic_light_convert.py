import pandas as pd
import ast

traffic_light_dict = {'go': '0', 'stop':'1'}
names =[]
df = pd.read_csv('traffic_light.csv')
for index, row in df.iterrows():
    names.append("data/obj/"+row['filename'])
    file_name = row['filename'].split('.')[0]
    object_class = ast.literal_eval(row['region_attributes'])['type']
    x = ast.literal_eval(row['region_shape_attributes'])['x']
    y = ast.literal_eval(row['region_shape_attributes'])['y']
    width = ast.literal_eval(row['region_shape_attributes'])['width']
    height = ast.literal_eval(row['region_shape_attributes'])['height']
    with open('labels/'+file_name+'.txt', 'a') as f:
        f.write(traffic_light_dict[object_class] + " " + str(x) + ' ' + str(y) + ' ' + str(width)+ ' ' + str(height))

with open('train.txt', 'w') as f:
    for item in names:
        f.write("%s\n" % item)