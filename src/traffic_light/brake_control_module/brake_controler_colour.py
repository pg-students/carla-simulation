import time
from loguru import logger as log
from src.NN_detection.detector.detector import Detector
from src.rgb_camera.image_data_server import ImageDataServer
import math
import matplotlib.pyplot as plt
import numpy as np
import skfuzzy as fuzz
import cv2


class BrakeControl_Color:
    """
    Class calculating suggested brake value for vehicle control based on visual detection of traffic lights using
    darknet yolov3-tiny

    To use you have to connect rgb camera listen method to image_data_server module in BrakeControl, example:
        camera.listen(BrakeControl.ImageDataServer.update_curr_image)

    """
    image_data_server = None
    detector = None
    brake_control_value = 0

    def __init__(self, time_wait_after_red, rgb_cam_bp, min_confidence=0.8, calculating_method="fuzzy"):
        """
        :param time_wait_after_red: How many seconds from end of a red light vehicle should wait to make sure
                                    that red light has definitely ended
        :param rgb_cam_bp: Blueprint of used rgb camera containing info about images resolution
        :param min_confidence: Minimal confidence of red light detection (0 - 0% confidence, 1 - 100% confidence)
        :param calculating_method: Chosen method to calculate brake value, methods available:
                "fuzzy" - calculating brake value from size of a bbox and its distance from center of an image using
                    fuzzy logic
        """
        self.detector = Detector()
        self.image_data_server = ImageDataServer()
        self.time_wait_after_red = time_wait_after_red
        self.last_stop_command = time.time()
        self.stop_command_occured = False
        self.cam_width = rgb_cam_bp.get_attribute("image_size_x").as_int()
        self.cam_height = rgb_cam_bp.get_attribute("image_size_y").as_int()
        self.min_confidence = min_confidence
        self.calculating_method = calculating_method

        if self.calculating_method == "fuzzy":
            max_bbox_area = self.cam_width * self.cam_height
            max_distance = self.cam_width/2
            self.bbox_area_range = np.arange(0, max_bbox_area, 1)
            self.bbox_fuzzy_func = fuzz.trapmf(self.bbox_area_range, [600, 900, max_bbox_area, max_bbox_area])
            # plt.plot(self.bbox_area_range, self.braking_estimate, 'b', linewidth=1.5, label='braking')
            # plt.show()
            self.distance_range = np.arange(0, max_distance, 1)
            self.distance_fuzzy_func = fuzz.trapmf(self.distance_range, [80, 130, max_distance, max_distance])

    def get_brake_control(self):
        """
        Calculate brake control from lastly updated image in Image data server
        :return: suggested brake value (from 0 to 1)
        """
        self.image = self.image_data_server.get_bgr_image()
        bbox, labels, conf, centers, _ = self.detector.detect(self.image)
        rgb = self.crop_bboxes(bbox, cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB))
        cropped = self.crop_bboxes(bbox, cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV))
        self.detect_red(cropped, rgb)


        best_conf = 0
        best_index = None
        for i, single_label in enumerate(labels):
            if single_label == "red" and conf[i] > self.min_confidence and conf[i] > best_conf:
                best_conf = conf[i]
                best_index = i

        if best_index is not None:
            best_detection = {"bbox": bbox[best_index],
                              "label": labels[best_index],
                              "conf": conf[best_index],
                              "center": centers[best_index]}

            if self.calculating_method == "fuzzy":
                distance = math.sqrt((self.cam_width / 2 - best_detection['center'][0]) ** 2 +
                                     (self.cam_height / 2 - best_detection['center'][1]) ** 2)
                bbox_area = (best_detection['bbox'][2] - best_detection['bbox'][0]) * (
                            best_detection['bbox'][3] - best_detection['bbox'][1])
                log.info(f"distance: {distance}, bbox area: {bbox_area}")

                self.brake_control_value = self.calculate_fuzzy_brake(bbox_area, distance)
            else:
                raise Exception("No method to calculate brake value has been chosen")

            if self.brake_control_value > 0:
                log.info(f"conf: {best_detection['conf']}")

                log.info("Braking recommended")
                self.last_stop_command = time.time()
                self.stop_command_occured = True

            return self.brake_control_value

        elif self.stop_command_occured and time.time() - self.last_stop_command > self.time_wait_after_red:
            log.info("Red light ended")
            self.stop_command_occured = False
            self.brake_control_value = 0
            return self.brake_control_value

        elif self.stop_command_occured and time.time() - self.last_stop_command < self.time_wait_after_red:
            return self.brake_control_value

        else:
            return 0

    def calculate_fuzzy_brake(self, bbox_area, distance):
        """
        Calculating brake value from size of a bbox and its distance from center of an image using
        fuzzy logic.
        :param bbox_area: area of a bounding box from last frame
        :param distance: distance of that bbox from center of an image
        :return: suggested brake value (from 0 to 1)
        """
        brake_from_bbox = fuzz.interp_membership(self.bbox_area_range, self.bbox_fuzzy_func, bbox_area)
        brake_from_distance = fuzz.interp_membership(self.distance_range, self.distance_fuzzy_func, distance)
        return np.fmin(brake_from_bbox, brake_from_distance)

    def crop_bboxes(self, bboxes, image):
        cropped = []
        for bbox in bboxes:
            cropped.append(image[bbox[1]:bbox[3], bbox[0]:bbox[2]])
            #plt.imshow(cropped[-1])
            #plt.show()
        return cropped

    def detect_red(self, cropped, rgbs):
        masks = []
        for cropp, rgb in zip(cropped, rgbs):
            mask1 = cv2.inRange(cropp, (0, 50, 20), (5, 255, 255))
            mask2 = cv2.inRange(cropp, (175, 50, 20), (180, 255, 255))
            mask3 = cv2.inRange(cropp, (161, 155, 84), (179, 255, 255))
            mask = cv2.bitwise_or(mask1, mask2)
            mask = cv2.bitwise_or(mask, mask3)
            red = cv2.bitwise_and(rgb, rgb, mask=mask)
            red_channel = red[:,:,0]
            red_pixels = np.nonzero(red_channel>80)
            plt.imshow(rgb)
            plt.show()
            plt.imshow(red)
            plt.show()
            try:
                y_min = np.amin(red_pixels[0])
                y_max = np.amax(red_pixels[0])
                x_min = np.amin(red_pixels[1])
                x_max = np.amax(red_pixels[1])
                height = y_max-y_min
                width = x_max-x_min
                roundy = height/width
                if 1.3<height<0.7:
                    log.info('dont care')
                else:
                    log.info('center')
                x = 5
            except:
                pass


