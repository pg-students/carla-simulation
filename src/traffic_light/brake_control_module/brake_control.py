import time
from src.traffic_light.detector.detector import Detector
from src.rgb_camera.image_data_server import ImageDataServer
import math
import numpy as np
import skfuzzy as fuzz


class BrakeControl:
    """
    Class calculating suggested brake value for vehicle control based on visual detection of traffic lights using
    darknet yolov3-tiny

    To use you have to connect rgb camera listen method to image_data_server module in BrakeControl, example:
        camera.listen(BrakeControl.ImageDataServer.update_curr_image)

    """
    _image_data_server = None
    _detector = None
    brake_control_value = 0

    def __init__(self, config_dict, time_wait_after_red, rgb_cam_actor, calculating_method, min_confidence=0.8):
        """
        :param time_wait_after_red: How many seconds from end of a red light vehicle should wait to make sure
                                    that red light has definitely ended
        :param rgb_cam_actor: Actor of used rgb camera
        :param min_confidence: Minimal confidence of red light detection (0 - 0% confidence, 1 - 100% confidence)
        :param calculating_method: Chosen method to calculate brake value, methods available:
                "fuzzy" - calculating brake value from size of a bbox and its distance from center of an image using
                    fuzzy logic
        """
        self._rgb_cam_actor = rgb_cam_actor
        self._detector = Detector(config_dict)
        self._image_data_server = ImageDataServer(rgb_cam_actor)

        self._time_wait_after_red = time_wait_after_red
        self._last_stop_command = time.time()
        self._stop_command_occured = False
        self._cam_width = int(rgb_cam_actor.attributes["image_size_x"])
        self._cam_height = int(rgb_cam_actor.attributes["image_size_y"])
        self._min_confidence = min_confidence
        self._calculating_method = calculating_method

        if self._calculating_method == "fuzzy":
            max_bbox_area = self._cam_width * self._cam_height
            max_distance = self._cam_width / 2
            self._bbox_area_range = np.arange(0, max_bbox_area, 1)
            self._bbox_fuzzy_func = fuzz.smf(self._bbox_area_range, 600, 900)
            self._distance_range = np.arange(0, max_distance, 1)
            self._distance_fuzzy_func = fuzz.smf(x=self._distance_range, a=80, b=130)

        # fig, ax0 = plt.subplots(figsize=(8, 3))
        # ax0.plot(self._bbox_area_range, self._bbox_fuzzy_func, 'b', linewidth=0.5)
        #
        # plt.tight_layout()
        # plt.xlabel("Area of bbox ")
        # plt.ylabel("Recommended brake value")
        # plt.show()
        #
        # fig, ax0 = plt.subplots(figsize=(8, 3))
        # ax0.plot(self._distance_range, self._distance_fuzzy_func, 'b', linewidth=0.5)
        #
        # plt.tight_layout()
        # plt.xlabel("Distance from center of bbox to center of image")
        # plt.ylabel("Recommended brake value")
        # plt.show()

    def __del__(self):
        self._rgb_cam_actor.destroy()
        del self

    def get_brake_value(self):
        """
        Method using chosen calculating method to calculate suggested brake value.
        :return: suggested brake value (from 0 to 1) using chosen method
        """
        if self._calculating_method == "fuzzy":
            return self._get_brake_using_fuzzy()

    def _get_brake_using_fuzzy(self):
        """
        Calculate brake control from lastly updated image in Image data server
        :return: suggested brake value (from 0 to 1)
        """
        bbox, labels, conf, centers = self._detector.get_detection(self._image_data_server.get_bgr_image())

        best_conf = 0
        best_index = None
        for i, single_label in enumerate(labels):
            if single_label == "red" and conf[i] > self._min_confidence and conf[i] > best_conf:
                best_conf = conf[i]
                best_index = i

        if best_index is not None:
            best_detection = {"bbox": bbox[best_index],
                              "label": labels[best_index],
                              "conf": conf[best_index],
                              "center": centers[best_index]}

            if self._calculating_method == "fuzzy":
                distance = math.sqrt((self._cam_width / 2 - best_detection['center'][0]) ** 2 +
                                     (self._cam_height / 2 - best_detection['center'][1]) ** 2)
                bbox_area = (best_detection['bbox'][2] - best_detection['bbox'][0]) * (
                        best_detection['bbox'][3] - best_detection['bbox'][1])

                self.brake_control_value = self._calculate_fuzzy_brake(bbox_area, distance)
            else:
                raise Exception("No method to calculate brake value has been chosen")

            if self.brake_control_value > 0:
                self._last_stop_command = time.time()
                self._stop_command_occured = True

            return self.brake_control_value

        elif self._stop_command_occured and time.time() - self._last_stop_command > self._time_wait_after_red:
            self._stop_command_occured = False
            self.brake_control_value = 0
            return self.brake_control_value

        elif self._stop_command_occured and time.time() - self._last_stop_command < self._time_wait_after_red:
            return self.brake_control_value

        else:
            return 0

    def _calculate_fuzzy_brake(self, bbox_area, distance):
        """
        Calculating brake value from size of a bbox and its distance from center of an image using
        fuzzy logic.
        :param bbox_area: area of a bounding box from last frame
        :param distance: distance of that bbox from center of an image
        :return: suggested brake value (from 0 to 1)
        """
        brake_from_bbox = fuzz.interp_membership(self._bbox_area_range, self._bbox_fuzzy_func, bbox_area)
        brake_from_distance = fuzz.interp_membership(self._distance_range, self._distance_fuzzy_func, distance)
        return np.fmin(brake_from_bbox, brake_from_distance)

    def get_detection(self):
        return self._detector.get_detection(self._image_data_server.get_bgr_image())

    def get_img_with_detection(self):
        return self._detector.get_img_with_detection(self._image_data_server.get_bgr_image())

    def get_bgr_img(self):
        return self._image_data_server.get_bgr_image()
