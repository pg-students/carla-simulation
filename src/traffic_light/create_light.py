import carla
import math


def create_race_start_light(reference_vehicle_transform, world, change_after=16):
    moved_traffic_yaw = math.radians(reference_vehicle_transform.rotation.yaw + 15)
    vehicle_yaw = math.radians(reference_vehicle_transform.rotation.yaw)

    move_light_by = 10
    light_location = reference_vehicle_transform.location + carla.Location(
        y=move_light_by * math.sin(moved_traffic_yaw), z=3,
        x=move_light_by * math.cos(moved_traffic_yaw))

    world.debug.draw_box(box=carla.BoundingBox(light_location, carla.Vector3D(0.2, 0.2, 1)),
                         rotation=reference_vehicle_transform.rotation,
                         thickness=0.4, color=carla.Color(0, 0, 0, 0), life_time=change_after * 2)

    world.debug.draw_box(
        box=carla.BoundingBox(
            light_location + carla.Location(y=-0.22 * math.sin(vehicle_yaw), x=-0.22 * math.cos(vehicle_yaw), z=0.8),
            carla.Vector3D(0.08, 0.08, 0.08)),
        rotation=reference_vehicle_transform.rotation,
        thickness=0.26, color=carla.Color(60, 60, 60, 0), life_time=change_after * 2)

    world.debug.draw_box(
        box=carla.BoundingBox(
            light_location + carla.Location(y=-0.22 * math.sin(vehicle_yaw), x=-0.22 * math.cos(vehicle_yaw), z=0.0),
            carla.Vector3D(0.08, 0.08, 0.08)),
        rotation=reference_vehicle_transform.rotation,
        thickness=0.26, color=carla.Color(60, 60, 60, 0), life_time=change_after * 2)

    world.debug.draw_box(
        box=carla.BoundingBox(
            light_location + carla.Location(y=-0.22 * math.sin(vehicle_yaw), x=-0.22 * math.cos(vehicle_yaw), z=-0.8),
            carla.Vector3D(0.08, 0.08, 0.08)),
        rotation=reference_vehicle_transform.rotation,
        thickness=0.26, color=carla.Color(0, 255, 0, 0), life_time=change_after * 2)

    world.debug.draw_box(
        box=carla.BoundingBox(
            light_location + carla.Location(y=-0.24 * math.sin(vehicle_yaw), x=-0.24 * math.cos(vehicle_yaw), z=0.8),
            carla.Vector3D(00.08, 0.08, 0.08)),
        rotation=reference_vehicle_transform.rotation,
        thickness=0.26, color=carla.Color(255, 0, 0, 0), life_time=change_after)

    world.debug.draw_box(
        box=carla.BoundingBox(
            light_location + carla.Location(y=-0.24 * math.sin(vehicle_yaw), x=-0.24 * math.cos(vehicle_yaw), z=-0.8),
            carla.Vector3D(0.08, 0.08, 0.08)),
        rotation=reference_vehicle_transform.rotation,
        thickness=0.26, color=carla.Color(60, 60, 60, 0), life_time=change_after)
