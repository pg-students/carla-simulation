import cv2
import numpy as np
import os
import sys
from loguru import logger

NN_MODEL_STATES_DIR = os.path.join(os.path.dirname(__file__), "nn_models")


class Detector:
    def __init__(self, config_dict):
        self.avg = list(np.zeros(6))
        self.net = None
        self.classes = None
        self.COLORS = np.random.uniform(0, 255, size=(80, 3))

        try:
            self.model = os.path.join(NN_MODEL_STATES_DIR, config_dict['detector']['model'])
            self.weights = os.path.join(NN_MODEL_STATES_DIR, config_dict['detector']['weights'])
            self.names = os.path.join(NN_MODEL_STATES_DIR, config_dict['detector']['names'])
            self.conf = config_dict['detector']['conf']
            self.nms = config_dict['detector']['nms']
            self.scale = config_dict['detector']['scale']
        except KeyError as e:
            logger.error(f'Some keys missing in Detector: {e}')
            sys.exit(1)

        self.initialize()

    def initialize(self):
        self.classes = self._populate_class_labels()
        self.net = cv2.dnn.readNet(self.weights, self.model)

    def _populate_class_labels(self):
        """
        This function extracts names of classes from input .txt file (each class has to be in separate line).
        :return:
            List of classes names.
        """
        with open(self.names, encoding="utf-8") as file:
            cl = [l.strip() for l in file]

        return cl

    def _get_output_layers(self):
        """
        This function returns names of neural network layers which have unconnected outputs.
        :return:
            List of layers names.
        """
        layer_names = self.net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

        return output_layers

    def _draw_bbox(self, img, bbox, labels, confidence, colors=None, write_conf=False):
        """
        This function uses neural network outputs to draw bounding boxes on processed images.
        :param img:
            Currently processed image.
        :param bbox:
            List of lists. Each inside list contains four coordinates needed to draw each bounding box.
        :param labels:
            List of labels ordered accordingly to the order of bounding boxes.
        :param confidence:
            List of confidence values for each detection.
        :param colors:
            List of bounding boxes layers. None by default.
        :param write_conf:
            Change value to True for writing confidence values next to classes names.
        :return:
            Image with proper bounding boxes.
        """
        if self.classes is None:
            self.classes = self._populate_class_labels()
        for i, label in enumerate(labels):
            if colors is None:
                color = self.COLORS[self.classes.index(label)]
            else:
                color = colors[self.classes.index(label)]
            # writing confidence (optional)
            if write_conf:
                label += ' ' + str(format(confidence[i] * 100, '.2f')) + '%'
            cv2.rectangle(img, (bbox[i][0], bbox[i][1]), (bbox[i][2], bbox[i][3]), color, 2)
            cv2.putText(img, label, (bbox[i][0], bbox[i][1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        return img

    def _calculate_nn_output(self, image):
        """
        This function processes outputs from neural network and orders them in proper containers.
        :param image:
            Currently processed image.
        :return:
            Lists of bounding boxes, labels and confidence values.
        """
        height, width = image.shape[:2]

        # scale implementation (self.scale), resizing
        blob = cv2.dnn.blobFromImage(image, self.scale, (416, 416), (0, 0, 0), True, crop=False)
        self.net.setInput(blob)
        outs = self.net.forward(self._get_output_layers())

        class_ids = []
        confidences = []
        boxes = []
        centers = []

        # confidence threshold
        conf_threshold = self.conf
        # non-maximum suppression threshold
        nms_threshold = self.nms

        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    centers.append([center_x, center_y])
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])

        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

        bbox = []
        label = []
        conf = []

        for i in indices:
            i = i[0]
            box = boxes[i]
            x = box[0]
            y = box[1]
            w = box[2]
            h = box[3]
            bbox.append([round(x), round(y), round(x+w), round(y+h)])
            label.append(str(self.classes[int(class_ids[i])]))
            conf.append(confidences[i])

        return bbox, label, conf, centers

    def get_detection(self, frame):
        bbox, label, conf, centers = self._calculate_nn_output(frame)
        return bbox, label, conf, centers

    def get_img_with_detection(self, frame):
        bbox, label, conf, centers = self._calculate_nn_output(frame)
        return self._draw_bbox(frame, bbox, label, conf)

    # def parse_args(self):
    #     """
    #     This function allows to change program parameters while running from command line.
    #     :return:
    #         Object containing values of all listed arguments.
    #     """
    #     parser = argparse.ArgumentParser()
    #
    #     parser.add_argument('--model', '-m', help='path to neural network model',
    #                         default=os.path.join(NN_model_states_dir, "obj-yolov3-tiny-2.cfg"))
    #     parser.add_argument('--weights', '-w', help='path to neural network weights',
    #                         default=os.path.join(NN_model_states_dir, 'obj-yolov3-tiny-2_104000.weights'))
    #     parser.add_argument('--names', '-n', help='path to file containing names of classes',
    #                         default=os.path.join(NN_model_states_dir, "obj.names.txt"))
    #     parser.add_argument('--conf', '-c', help='confidence threshold value', type=float,
    #                         default=0.4)
    #     parser.add_argument('--nms', '-t', help='non-maximum suppression threshold value', type=float,
    #                         default=0.9)
    #     parser.add_argument('--scale', '-s', help='scale of input frames', type=float,
    #                         default=0.01)
    #
    #     args = parser.parse_args()
    #
    #     self.model = args.model
    #     self.weights = args.weights
    #     self.names = args.names
    #     self.conf = args.conf
    #     self.nms = args.nms
    #     self.scale = args.scale
