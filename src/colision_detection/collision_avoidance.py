import glob
import os
import sys
import random
import time
import numpy as np
import math
import copy

from src.colision_detection.sensor_data import get_sensor_data

from carla import Transform, Rotation, Location, VehicleControl


class CollisionDetector:
    def __init__(self, world, vehicle, throttle=0.5, steer=1.0, frames=3, ctrl_time=10.0, is_auto=False):
        self.world = world
        self.vehicle = vehicle

        self.is_autonomous = is_auto

        self.blueprint_library = self.world.get_blueprint_library()
        self.sensors_data = get_sensor_data()
        self.control = VehicleControl()        

        self.collision_hist = []
        self.sensors_list = {}
        self.actor_list = []

        self.isDestroyed = False
        self.is_collision_detected = False

        self.front_row_mid = [False, False, False]
        self.front_row_near = [False, False, False]
        self.front_row_hs = [False, False, False, False, False]
        self.hs_crash = [False, False]
        self.right_side = False
        self.left_side = False

        self.front_row_mid_frame = [0, 0, 0]
        self.front_row_near_frame = [0, 0, 0]
        self.front_row_hs_frame = [0, 0, 0, 0, 0]
        self.hs_crash_frame = [0, 0]
        self.right_side_frame = 0
        self.left_side_frame = 0

        self.throttle_amt = throttle
        self.steer_amt = steer

        self.frames_margin = frames

        self.controle_time = ctrl_time

        self.frames_in_control = 0

        self.kmh = 0

    def destroy(self):
        self.isDestroyed = True
        for actor in self.actor_list:
            actor.destroy()
        print("Cleaned")

    def run(self):
        self.add_collision_sensor()
        self.add_obstacle_sensors()
        self.world.on_tick(lambda data: self.control_loop(data))

    def add_collision_sensor(self):
        self.sensors_list['collision'] = self.world.spawn_actor(
            self.blueprint_library.find("sensor.other.collision"),
            Transform(Location(z=0.7, x=2.5)), attach_to=self.vehicle)
        self.actor_list.append(self.sensors_list['collision'])
        self.sensors_list['collision'].listen(lambda event: self.handle_collision(event))

    def add_obstacle_sensors(self):
        self.prepare_obstacle_sensors()

        self.sensors_list['front left'].listen(lambda event: self.collision_data_fl(event))
        self.sensors_list['front middle'].listen(lambda event: self.collision_data_mid(event))
        self.sensors_list['front right'].listen(lambda event: self.collision_data_fr(event))

        self.sensors_list['front left close'].listen(lambda event: self.collision_data_fl_close(event))
        self.sensors_list['front right close'].listen(lambda event: self.collision_data_fr_close(event))
        self.sensors_list['front mid close'].listen(lambda event: self.collision_data_mid_close(event))
        
        self.sensors_list['front left 1 high speed'].listen(lambda event: self.collision_data_hs_f1(event))
        self.sensors_list['front left 2 high speed'].listen(lambda event: self.collision_data_hs_f1_2(event))
        self.sensors_list['front middle high speed'].listen(lambda event: self.collision_data_hs_mid(event))
        self.sensors_list['front right 1 high speed'].listen(lambda event: self.collision_data_hs_fr(event))
        self.sensors_list['front right 2 high speed'].listen(lambda event: self.collision_data_hs_fr_2(event))

        self.sensors_list['high speed crash'].listen(lambda event: self.collision_data_hs_crash(event))
        self.sensors_list['super high speed crash'].listen(lambda event: self.collision_data_shs_crash(event))

        self.sensors_list['right_side 1'].listen(lambda event: self.collision_data_right_side(event))
        self.sensors_list['right_side 2'].listen(lambda event: self.collision_data_right_side(event))
        self.sensors_list['right_side 3'].listen(lambda event: self.collision_data_right_side(event))

        self.sensors_list['left_side 1'].listen(lambda event: self.collision_data_left_side(event))
        self.sensors_list['left_side 2'].listen(lambda event: self.collision_data_left_side(event))
        self.sensors_list['left_side 3'].listen(lambda event: self.collision_data_left_side(event))

    def prepare_obstacle_sensors(self):
        for sensor_name in self.sensors_data:
            sensor = self.sensors_data[sensor_name]
            blueprint = self.blueprint_library.find("sensor.other.obstacle")
            blueprint = self.sensor_set_many_attributes(blueprint, sensor['bp_attributes'])
            transform = self.get_transform(sensor['location'], sensor['rotation'])
            self.sensors_list[sensor_name] = self.world.spawn_actor(blueprint, transform, attach_to=self.vehicle)
            self.actor_list.append(self.sensors_list[sensor_name])

    def sensor_set_many_attributes(self, bp, attributes):
        for attribute in attributes:
            bp.set_attribute(attribute, attributes[attribute])
        return bp

    def get_transform(self, loc, rot):
        location = Location(x=loc['x'], y=loc['y'], z=loc['z'])
        rotation = Rotation(pitch=rot['pitch'], yaw=rot['yaw'], roll=rot['roll'])
        transform = Transform(location, rotation)
        return transform

    def handle_collision(self, _):
        self.set_control(brake=1.0)
        print("COLLISION")

    def get_obstacle_with_sensor_attribute(self, attribute_list):
        blueprint = self.blueprint_library.find("sensor.other.collision")
        for attribute in attribute_list:
            blueprint.set_attribute(attribute)
        return blueprint

    def get_data_from_event(self, event):
        str_data = str(event) \
            .replace('ObstacleDetectionEvent(frame=', '') \
            .replace(', timestamp=', ' ') \
            .replace(', other_actor=', ' ')
        arr_data = str_data.split(' ')

        frame = int(arr_data[0])
        timestamp = arr_data[1]
        other_actor = arr_data[2]
        return frame

    def collision_data_hs_crash(self, event):
        self.hs_crash[0] = True
        self.hs_crash_frame[0] = self.get_data_from_event(event)

    def collision_data_shs_crash(self, event):
        self.hs_crash[1] = True
        self.hs_crash_frame[1] = self.get_data_from_event(event)

    def collision_data_fl(self, event):
        self.front_row_mid[0] = True
        self.front_row_mid_frame[0] = self.get_data_from_event(event)

    def collision_data_fl_close(self, event):
        self.front_row_near[0] = True
        self.front_row_near_frame[0] = self.get_data_from_event(event)

    def collision_data_mid(self, event):
        self.front_row_mid[1] = True
        self.front_row_mid_frame[1] = self.get_data_from_event(event)

    def collision_data_mid_close(self, event):
        self.front_row_near[1] = True
        self.front_row_near_frame[1] = self.get_data_from_event(event)

    def collision_data_fr_close(self, event):
        self.front_row_near[2] = True
        self.front_row_near_frame[2] = self.get_data_from_event(event)

    def collision_data_fr(self, event):
        self.front_row_mid[2] = True
        self.front_row_mid_frame[2] = self.get_data_from_event(event)

    def collision_data_hs_f1(self, event):
        self.front_row_hs[0] = True
        self.front_row_hs_frame[0] = self.get_data_from_event(event)

    def collision_data_hs_f1_2(self, event):
        self.front_row_hs[1] = True
        self.front_row_hs_frame[1] = self.get_data_from_event(event)

    def collision_data_hs_mid(self, event):
        self.front_row_hs[2] = True
        self.front_row_hs_frame[2] = self.get_data_from_event(event)

    def collision_data_hs_fr_2(self, event):
        self.front_row_hs[3] = True
        self.front_row_hs_frame[3] = self.get_data_from_event(event)

    def collision_data_hs_fr(self, event):
        self.front_row_hs[4] = True
        self.front_row_hs_frame[4] = self.get_data_from_event(event)

    def collision_data_right_side(self, event):
        self.right_side = True
        self.right_side_frame = self.get_data_from_event(event)

    def collision_data_left_side(self, event):
        self.left_side = True
        self.left_side_frame = self.get_data_from_event(event)

    def control_loop(self, event):
        if not self.isDestroyed:
            self.set_velocity()

            word_frame = self.world.wait_for_tick().frame_count
            self.clear_sensor_data(word_frame)

            self.process_control()

            self.set_colision_state()

    def set_velocity(self):    
        V = self.vehicle.get_velocity()
        self.kmh = int(3.6 * math.sqrt(V.x ** 2 + V.y ** 2 + V.z ** 2))

    def process_control(self):
        if self.kmh < 25:
            self.control_normal_speed()
        else:
            self.control_high_speed()

    def control_high_speed(self):
        if self.kmh > 50 and self.hs_crash[1]:
            self.set_control(brake=1.0,)
        if self.kmh > 30 and self.hs_crash[0]:
            self.set_control(brake=1.0)
        else:
            self.process_sides()
            self.process_high_speed()
            self.process_middle()
            self.process_nearest()
            

    def control_normal_speed(self):
        self.process_sides()
        self.process_middle()
        self.process_nearest()

    def set_colision_state(self):
        if self.frames_in_control > self.controle_time:
            state = False
            for elem in self.front_row_mid:
                if elem:
                    state = True
                    break

            if not state:
                for elem in self.front_row_hs:
                    if elem:
                        state = True
                        break

            if not state:
                for elem in self.front_row_near:
                    if elem:
                        state = True
                        break

            if not state:
                for elem in self.hs_crash:
                    if elem:
                        state = True
                        break

            if self.right_side:
                state = True

            if self.right_side:
                state = True

            if not state:
                self.is_collision_detected = False
        else:
            self.frames_in_control += 1

    def clear_sensor_data(self, word_frame):
        for i in range(len(self.front_row_mid)):
            if (self.front_row_mid_frame[i] + self.frames_margin) < word_frame:
                self.front_row_mid[i] = False

        for i in range(len(self.front_row_hs)):
            if (self.front_row_hs_frame[i] + self.frames_margin) < word_frame:
                self.front_row_hs[i] = False

        for i in range(len(self.front_row_near)):
            if (self.front_row_near_frame[i] + self.frames_margin) < word_frame:
                self.front_row_near[i] = False
        
        for i in range(len(self.hs_crash)):
            if (self.hs_crash_frame[i] + self.frames_margin) < word_frame:
                self.hs_crash[i] = False

        if (self.right_side_frame + self.frames_margin) < word_frame:
                self.right_side = False

        if (self.left_side_frame + self.frames_margin) < word_frame:
                self.left_side = False

    def process_sides(self):
        if self.left_side or self.right_side:
            self.set_control(throttle=self.throttle_amt)


    def process_nearest(self):
        # Check closest front sensors
        if self.front_row_near[1]:
            if self.front_row_near[0] and not self.front_row_near[2]:
                if not self.right_side:
                    self.set_control(throttle=self.throttle_amt, steer=1.5 * self.steer_amt)
                else:
                    self.set_control(steer=2 * self.steer_amt, throttle=0.5 * self.throttle_amt)  #brake=1.0)
            elif not self.front_row_near[0] and self.front_row_near[2]:
                if not self.left_side:
                    self.set_control(throttle=self.throttle_amt, steer=-1.5 * self.steer_amt)
                else:
                    self.set_control(steer=-2 * self.steer_amt, throttle=0.5 * self.throttle_amt)  #brake=1.0)
            elif self.front_row_near[0] and self.front_row_near[2]:
                self.set_control(brake=1.0)
        else:
            if self.front_row_near[0] and not self.front_row_near[2]:
                if not self.right_side:
                    self.set_control(throttle=self.throttle_amt, steer=0.3 * self.steer_amt)
                else:
                    self.set_control(steer=self.steer_amt, throttle=0.5 * self.throttle_amt)  #brake=1.0)
            elif not self.front_row_near[0] and self.front_row_near[2]:
                if not self.left_side:
                    self.set_control(throttle=self.throttle_amt, steer=-0.3 * self.steer_amt)
                else:
                    self.set_control(steer=-1 * self.steer_amt, throttle=0.5 * self.throttle_amt)  #brake=1.0)
            elif self.front_row_near[0] and self.front_row_near[2]:
                self.set_control(brake=1.0)

    def process_middle(self):
        # Control vehicle when when obstacle is in front
        if self.front_row_mid[1]:
            if self.front_row_mid[2] and not self.front_row_mid[0]:
                if not self.left_side:
                    if self.kmh < 20:
                        self.set_control(throttle=self.throttle_amt, steer=-0.8 * self.steer_amt)
                    else:
                        self.set_control(steer=-1 * self.steer_amt, brake=0.5)
                else:
                    self.set_control(steer=-0.8 * self.steer_amt, brake=1.0)
            elif self.front_row_mid[0] and not self.front_row_mid[2]:
                if not self.right_side:
                    if self.kmh < 20:
                        self.set_control(throttle=self.throttle_amt, steer=0.8 * self.steer_amt)
                    else:
                        self.set_control(steer=1 * self.steer_amt, brake=0.5)
                else:
                    self.set_control(steer=0.8 * self.steer_amt, brake=1.0)
            elif self.front_row_mid[0] and self.front_row_mid[2]:
                if not self.left_side:
                    self.set_control(steer=-0.8 * self.steer_amt)
                elif not self.right_side:
                    self.set_control(steer=0.8 * self.steer_amt)
                else:
                    self.set_control(brake=1.0)

        # Control vehicle when there is no obstacle in the front
        else:
            if self.front_row_mid[2] and not self.front_row_mid[0]:
                if not self.left_side:
                    if self.kmh < 20:
                        self.set_control(throttle=self.throttle_amt, steer=-0.6 * self.steer_amt)
                    else:
                        self.set_control(steer=-1 * self.steer_amt, brake=0.5)
                else:
                    self.set_control(brake=0.5)

            elif self.front_row_mid[0] and not self.front_row_mid[2]:
                if not self.right_side:
                    if self.kmh < 20:
                        self.set_control(throttle=self.throttle_amt, steer=0.6 * self.steer_amt)
                    else:
                        self.set_control(steer=1 * self.steer_amt, brake=0.5)
                else:
                    self.set_control(brake=0.5)      
            else:                
                self.set_control(throttle=self.throttle_amt, is_colision=False)

    def process_high_speed(self):
        if self.front_row_hs[2]:
            if self.front_row_hs[0] or self.front_row_hs[1]:
                if self.front_row_hs[3] and not self.front_row_hs[4]:
                    if self.kmh < 50:
                        self.set_control(throttle=self.throttle_amt, steer=0.3 * self.steer_amt)
                    else:
                        self.set_control(throttle=self.throttle_amt, steer=0.6 * self.steer_amt)
                elif not self.front_row_hs[3] and not self.front_row_hs[4]:
                    if self.kmh < 50:
                        self.set_control(throttle=self.throttle_amt, steer=0.15 * self.steer_amt)
                    else:
                        self.set_control(throttle=self.throttle_amt, steer=0.3 * self.steer_amt)
                else:
                    self.set_control(brake=1.0)

            elif self.front_row_hs[3] or self.front_row_hs[4]:
                if not self.front_row_hs[0] and self.front_row_hs[1]:
                    if self.kmh < 50:
                        self.set_control(throttle=self.throttle_amt, steer=-0.3 * self.steer_amt)
                    else:
                        self.set_control(throttle=self.throttle_amt, steer=-0.6 * self.steer_amt)
                elif not self.front_row_hs[0] and not self.front_row_hs[1]:
                    if self.kmh < 50:
                        self.set_control(throttle=self.throttle_amt, steer=-0.15 * self.steer_amt)
                    else:
                        self.set_control(throttle=self.throttle_amt, steer=-0.3 * self.steer_amt)
                else:
                    self.set_control(brake=1.0)
        else:
            if not self.front_row_hs[1] and not self.front_row_hs[3]:
                self.set_control(throttle=self.throttle_amt, is_colision=False)
            elif not self.front_row_hs[0] and not self.front_row_hs[4]:
                self.set_control(throttle=self.throttle_amt, is_colision=False)
            elif self.front_row_hs[0] and not self.front_row_hs[1] and not self.front_row_hs[3]:
                self.set_control(throttle=self.throttle_amt, steer=0.1 * self.steer_amt)
            elif not self.front_row_hs[0] and not self.front_row_hs[1] and self.front_row_hs[4]:
                self.set_control(throttle=self.throttle_amt, steer=-0.1 * self.steer_amt)
            elif self.front_row_hs[0] and self.front_row_hs[1] and not self.front_row_hs[3] and not self.front_row_hs[4]:
                self.set_control(throttle=self.throttle_amt, steer=0.2 * self.steer_amt)
            elif not self.front_row_hs[0] and not self.front_row_hs[1] and self.front_row_hs[3] and self.front_row_hs[4]:
                self.set_control(throttle=self.throttle_amt, steer=-0.2 * self.steer_amt)

    def set_control(self, throttle=0.0, brake=0.0, steer=0.0, is_colision=True):
        if is_colision:
            self.frames_in_control = 0
            self.is_collision_detected = True
        else:
            self.is_collision_detected = False
        self.control.throttle = throttle
        self.control.brake = brake
        self.control.steer = steer
        if self.is_autonomous:
            self.vehicle.apply_control(VehicleControl(throttle=throttle, brake=brake, steer=steer))

    def get_control(self):
        return copy.copy(self.control)
