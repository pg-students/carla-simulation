
def get_sensor_data():
    return {
        'row 1 right': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 15,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 1 left': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -15,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 2 middle': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '10'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 2 right': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '10'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 10,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 2 left': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '10'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -10,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 3 middle': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '20'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 0,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 3 right': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '20'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 5,
                'pitch': 0,
                'roll': 0
            }
        },
        'row 3 left': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '20'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -5,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 1': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 45,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 2': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '2.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 90,
                'pitch': 0,
                'roll': 0
            }
        },
        'right_side 3': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': 135,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 1': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -45,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 2': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '2.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -90,
                'pitch': 0,
                'roll': 0
            }
        },
        'left_side 3': {
            'bp_attributes': {
                'debug_linetrace': 'false',
                'hit_radius': '1.0',
                'distance': '3.5'
            },
            'location': {
                'z': 1.2,
                'x': 0,
                'y': 0
            },
            'rotation': {
                'yaw': -135,
                'pitch': 0,
                'roll': 0
            }
        },
    }


