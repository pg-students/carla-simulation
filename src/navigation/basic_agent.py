""" This module implements an agent that roams around a track following waypoints. """


import carla
from loguru import logger as log
from src.navigation.agent import Agent, AgentState
from src.navigation.local_planner import LocalPlanner
from src.base_routing.guide_class import Guide


class BasicAgent(Agent):
    """
    BasicAgent implements a basic agent that navigates scenes to reach a given
    target destination. This agent respects traffic lights and other vehicles.
    """

    def __init__(self, vehicle, navigation_algorithm, controller_type="stanley", target_speed=20,
                 anti_collision_sys=None, traffic_light_detection_system=None,
                 draw_path=False, path_color=carla.Color(r=255, g=0, b=0),
                 logs=False, **algorithm_kwargs):
        """
        :param vehicle: actor to apply to local planner logic onto
        """
        super(BasicAgent, self).__init__(vehicle)

        self._navigation_algorithm = navigation_algorithm
        self._algorithm_kwargs = algorithm_kwargs
        self._proximity_threshold = 10.0  # meters
        self._state = AgentState.NAVIGATING
        args_lateral_dict = {
            'K_P': 0.7,
            'K_D': 0.01,
            'K_I': 0.2,
            'dt': 1.0/20.0}
        args_stanley_dict = {
            'K': 2.5,
            'Delta': 0.05,
            'EXP_DECAY': 0.1,
            }
        self._world.debug.draw_string(self._map.get_spawn_points()[125].location, 'PG ssie', draw_shadow=True,
                          color=carla.Color(r=255, g=10, b=100), life_time=15.0, persistent_lines=True)

        self._local_planner = LocalPlanner(
            self._vehicle, opt_dict={'target_speed': target_speed, 'lateral_control_dict': args_lateral_dict,
                                     'stanley_control_dict': args_stanley_dict, 'controller_type': controller_type})
        self._hop_resolution = 2.0
        self._path_seperation_hop = 2
        self._path_seperation_threshold = 0.5
        self._target_speed = target_speed      
        self._grp = None
        self._ac_sys = anti_collision_sys
        self._tld_sys = traffic_light_detection_system
        self._draw_path = draw_path
        self._path_color = path_color
        self.logs = logs

    def stop_lights_detection(self):
        del self._tld_sys
        self._tld_sys = None

    def check_tld_status(self) -> bool:
        return bool(self._tld_sys)

    def set_destination(self, location):
        """
        This method creates a list of waypoints from agent's position to destination location
        based on the route returned by the global router
        """

        start_waypoint = self._map.get_waypoint(self._vehicle.get_location())
        end_waypoint = self._map.get_waypoint(carla.Location(location))

        log.info(f'Chosen navigation algorithm: {self._navigation_algorithm}')
        guide = Guide(start_waypoint, end_waypoint)
        route_trace = getattr(guide, self._navigation_algorithm)(**self._algorithm_kwargs)

        assert route_trace

        if(self._draw_path):
            for i in route_trace:
                self._world.debug.draw_string(i.transform.location, 'O', draw_shadow=False,
                                              color=self._path_color, life_time=120.0,
                                              persistent_lines=True)

        self._local_planner.set_global_plan(route_trace)

    def run_step(self, debug=False):
        """
        Execute one step of navigation.
        :return: carla.VehicleControl
        """
        self._state = AgentState.NAVIGATING

        #
        #   Our control tree
        #

        control = None

        if self._ac_sys is not None:
            if self._ac_sys.is_collision_detected:
                control = self._ac_sys.control
                if self.logs:
                    print('Anti collision system takes control')
        if control is None:
            control = self._local_planner.run_step()

        if self._tld_sys is not None:
            recommended_brake = self._tld_sys.get_brake_value()
            if recommended_brake:
                control.brake = recommended_brake
                control.throttle = 0
                if self.logs:
                    print('Traffic lights detection system takes control')

        return control
