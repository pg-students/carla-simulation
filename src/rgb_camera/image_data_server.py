import carla
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as PILImage
from copy import deepcopy
import time


class ImageDataServer(object):   # "object" z dedykacją dla Piotrka
    _curr_image = None

    def __init__(self, rgb_cam_actor=None):
        if rgb_cam_actor is not None:
            self.connect_to_camera(rgb_cam_actor)

    def connect_to_camera(self, rgb_cam_actor):
        """
        Connecting listed method of camera with Data Server class
        :param rgb_cam_actor: object of RGB Camera actor
        :return:
        """
        rgb_cam_actor.listen(self._update_curr_image)
        print(f"Waiting for {1.5 * float(rgb_cam_actor.attributes['sensor_tick'])} seconds (1.5 * RGB Camera tick) to "
              f"get first image from camera")
        time.sleep(1.5 * float(rgb_cam_actor.attributes['sensor_tick']))

    def _update_curr_image(self, new_image):
        """
        Method to pass to rgb_camera.listen() to provide up to date images
        :param new_image: carla.Image object
        :return: None
        """
        self._curr_image = new_image

    def get_carla_image(self):
        """
        :return: currently stored carla.Image object
        """
        return self._curr_image

    def get_rgba_image(self):
        """
        :return: RGBA image as numpy array generated from raw data from currently stored carla.Image object
        """
        return deepcopy(self._to_rgba_array(self._curr_image))

    def get_rgb_image(self):
        """
        :return: RGB image as numpy array generated from raw data from currently stored carla.Image object
        """
        return deepcopy(self._to_rgb_array(self._curr_image))

    def get_bgra_image(self):
        """
        :return: BGRA image as numpy array generated from raw data from currently stored carla.Image object
        """
        return deepcopy(self._to_bgra_array(self._curr_image))

    def get_bgr_image(self):
        """
        :return: BGR image as numpy array generated from raw data from currently stored carla.Image object
        """
        return deepcopy(self._to_bgr_array(self._curr_image))

    # def get_PIL_RGB_image(self):
    #     """
    #     :return: Pillow RGB image generated from raw data from currently stored carla.Image object
    #     """
    #     return deepcopy(self.to_PIL_image(self._curr_image))

    def save_image(self, img, path_to_dir):
        """
        Saves carla.Image as RGB png image to chosen directory
        :param img: carla.Image object
        :param path_to_dir: path to directory in which image will be saved
        :return: None
        """
        # img.save_to_disk(f"{path_to_dir}{img.timestamp}.png")   # Python 3.7
        # img.save_to_disk("{}{}.png".format(path_to_dir, img.timestamp))
        img.save_to_disk(path_to_dir + str(img.timestamp) + ".png")

    @staticmethod
    def plot_image(img_array):
        """
        Plots input image using matplotlib.pyplot
        :param img_array: input image as numpy array
        :return: None
        """
        plt.imshow(img_array)
        plt.show()

    def _to_bgra_array(self, carla_image):
        """
        Converts raw data from input carla.Image to BGRA image as numpy array
        :param carla_image: carla.Image object
        :return: numpy array with BGRA image
        """
        img = np.frombuffer(carla_image.raw_data, dtype=np.dtype("uint8"))
        img = np.reshape(img, (carla_image.height, carla_image.width, 4))
        return img

    def _to_bgr_array(self, carla_image):
        """
        Converts raw data from input carla.Image to BGR image as numpy array
        :param carla_image: carla.Image object
        :return: numpy array with BGR image
        """
        img = self._to_bgra_array(carla_image)
        return img[:, :, :3]

    def _to_rgba_array(self, carla_image):
        """
        Converts raw data from input carla.Image to RGBA image as numpy array
        :param carla_image: carla.Image object
        :return: numpy array with RGBA image
        """
        img = self._to_bgra_array(carla_image)
        return np.dstack((img[:, :, :3][:, :, ::-1], img[:, :, 3]))

    def _to_rgb_array(self, carla_image):
        """
        Converts raw data from input carla.Image to RGB image as numpy array
        :param carla_image: carla.Image object
        :return: numpy array with RGB image
        """
        img = self._to_bgr_array(carla_image)
        return img[:, :, ::-1]

    # def to_PIL_image(self, carla_image):
    #     """
    #     Converts raw data from input carla.Image to RGB Pillow image
    #     :param carla_image: carla.Image object
    #     :return: RGB image as Pillow image
    #     """
    #     img = PILImage.frombuffer(
    #         mode='RGBA',
    #         size=(carla_image.width, carla_image.height),
    #         data=carla_image.raw_data,
    #         decoder_name='raw')
    #     b, g, r, _ = img.split()
    #     img = PILImage.merge("RGB", (r, g, b))
    #     return img
