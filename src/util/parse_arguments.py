import argparse


def parse_arguments():
    argparser = argparse.ArgumentParser(description='CARLA argumenty do kolizji')
    argparser.add_argument('-th', '--throttle', type=float, default=0.5)
    argparser.add_argument('-st', '--steer', type=float, default=1.0)
    argparser.add_argument('-fr', '--frames', type=int, default=3)
    argparser.add_argument('-ti', '--time', type=float, default=10.0)
    argparser.add_argument('-vs', '--vehicle_start', type=int, default=0)
    argparser.add_argument('-vd', '--vehicle_destination', type=int, default=10)
    argparser.add_argument('-ls1', '--lights_start_one', type=int, default=0)
    argparser.add_argument('-ls2', '--lights_start_two', type=int, default=10)

    args = argparser.parse_args()
    return args


if __name__ == "__main__":
    parse_arguments()
