import carla


def destroy_actors(client=None):
    if not client:
        client = carla.Client('localhost', 2000)
        client.set_timeout(10.0)  # seconds
    world = client.get_world()

    for actor in world.get_actors():
        if "vehicle." in actor.type_id or "sensor." in actor.type_id:
            print(f"Destroying actor {actor.type_id}")
            actor.destroy()


if __name__ == "__main__":
    destroy_actors()
