import argparse

import carla
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def main():
    parser = parse_args()
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds

    world = client.get_world()
    map_ = world.get_map()
    all_waypoints = map_.generate_waypoints(parser.map_resolution)

    fig = plt.figure()
    if parser.plot_in_3d:
        print("Plotting in 3D")
        ax = fig.add_subplot(111, projection='3d')
        for way_point in all_waypoints:
            ax.scatter(way_point.transform.location.x, way_point.transform.location.y, way_point.transform.location.z,
                       s=0.2, c="b")
    else:
        print("Plotting in 2D")
        ax = fig.add_subplot(111)
        for way_point in all_waypoints:
            ax.scatter(way_point.transform.location.x, way_point.transform.location.y, s=0.2, c="b")
    plt.show()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--map-resolution', '-res', dest="map_resolution",
                        help='Distance between waypoints on generated map', type=float,
                        required=False, default=10)
    parser.add_argument(
        '--plot-in-3d', dest="plot_in_3d", help="Set that flag if you want to plot map in 3D instead of 2D",
        required=False, action='store_true', default=False)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
