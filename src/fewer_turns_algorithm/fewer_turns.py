import math

import carla
import matplotlib.pyplot as plt

waypoints_range = 5
finish_approximation = 5

# TODO: Warning
"""
Newest version of algorithm is in Guide class in src/base_routing/guide_class.py
"""


def main():
    print("Connecting")
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)
    print("Processing")

    world = client.get_world()
    map_ = world.get_map()
    all_waypoints = map_.generate_waypoints(waypoints_range)

    start_point = find_closest_waypoint(all_waypoints, x=-50, y=-130)
    end_point = find_closest_waypoint(all_waypoints, x=200, y=3)

    route, crosses = biggest_forward_route(start_point, end_point, all_waypoints)

    print("Plotting")
    for point in all_waypoints:
        loc = point.transform.location
        plt.scatter(loc.x, loc.y, s=0.2, c="b")
    for wp in route:
        plt.scatter(wp.transform.location.x, wp.transform.location.y, s=[10], c="g")
    plt.scatter(start_point.transform.location.x, start_point.transform.location.y, s=[10], c="r")
    plt.scatter(end_point.transform.location.x, end_point.transform.location.y, s=[10], c="k")
    for cr in crosses:
        plt.scatter(cr.transform.location.x, cr.transform.location.y, s=[20], c="c")
    plt.show()


def find_closest_waypoint(list_of_waypoints, x, y):
    best_dist = math.inf
    best_waypoint = list_of_waypoints[0]
    for waypoint in list_of_waypoints:
        x_distance = waypoint.transform.location.x - x
        y_distance = waypoint.transform.location.y - y
        dist = math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))
        if dist < best_dist:
            best_dist = dist
            best_waypoint = waypoint
    return best_waypoint


def heuristic_distance_a_star(waypoint1, waypoint2):
    # Distance between two waypoints
    x_distance = waypoint1.transform.location.x - waypoint2.transform.location.x
    y_distance = waypoint1.transform.location.y - waypoint2.transform.location.y
    return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))


def find_frontal_wp(available_next_wps, curr_point):
    frontal_idx = None
    min_angle_dif = math.inf
    for idx, next_wp in enumerate(available_next_wps):
        angle_dif = abs(next_wp.transform.rotation.yaw - curr_point.transform.rotation.yaw)
        if angle_dif < 0.5 and angle_dif < min_angle_dif:
            frontal_idx = idx
            min_angle_dif = angle_dif

    return frontal_idx


def check_is_front_available(curr_waypoint, end_point):
    is_front_available = True
    old_distance = heuristic_distance_a_star(curr_waypoint, end_point)
    available_wp_after_next = curr_waypoint.next(1.2 * waypoints_range)
    while len(available_wp_after_next) == 1:
        curr_waypoint = available_wp_after_next[0]
        new_distance = heuristic_distance_a_star(curr_waypoint, end_point)
        if new_distance < waypoints_range + finish_approximation:
            return True
        elif new_distance < old_distance:
            old_distance = new_distance
            available_wp_after_next = curr_waypoint.next(1.2 * waypoints_range)
            continue
        else:
            is_front_available = False
            break

    return is_front_available


def biggest_forward_route(start_waypoint, end_waypoint, all_waypoints):
    route = [start_waypoint]
    crosses = []
    curr_waypoint = start_waypoint
    while True:
        surrounding_waypoinst = []
        for wp in all_waypoints:
            if heuristic_distance_a_star(curr_waypoint, wp) <= 2.0 * waypoints_range:
                surrounding_waypoinst.append(wp)
        possible_next_waypoints = curr_waypoint.next(1.2 * waypoints_range)

        if len(possible_next_waypoints) > 1:
            crosses.append(curr_waypoint)

            front_wp_idx = find_frontal_wp(possible_next_waypoints, curr_waypoint)
            if front_wp_idx is not None:
                is_front_available = check_is_front_available(possible_next_waypoints[front_wp_idx], end_waypoint)
            else:
                is_front_available = False
            if is_front_available:
                print("Going forward")
                must_go_wp = possible_next_waypoints[front_wp_idx].next(1.2 * waypoints_range)[0]
                for i in range(3):
                    route.append(must_go_wp)
                    if heuristic_distance_a_star(end_waypoint, must_go_wp) < waypoints_range + finish_approximation:
                        break
                    must_go_wp = must_go_wp.next(1.2 * waypoints_range)[0]
                curr_waypoint = must_go_wp
                if heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:
                    break
            else:
                # for each_next in possible_nexts:
                #     next_nexts = each_next.next(1.2 * waypoints_range)
                #     while len(next_nexts) == 1:
                #         nextes.append(next_nexts[0])
                #         next_nexts = next_nexts[0].next(1.2 * waypoints_range)
                #     end_nextes.append(next_nexts[0])

                new_start = find_closest_to_end_wp(surrounding_waypoinst, end_waypoint)

                route.append(new_start)
                curr_waypoint = new_start
                if heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:
                    break
        else:
            new_start = find_closest_to_end_wp(surrounding_waypoinst, end_waypoint)
            route.append(new_start)
            curr_waypoint = new_start
            if heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:
                break
        print(f"Curr distance: {heuristic_distance_a_star(end_waypoint, curr_waypoint)}")
    return route, crosses


def find_closest_to_end_wp(surrounding_waypoinst, end_waypoint):
    smallest_distance = math.inf
    new_start = None
    for waypoint in surrounding_waypoinst:
        distance = heuristic_distance_a_star(end_waypoint, waypoint)
        if distance < smallest_distance:
            smallest_distance = distance
            new_start = waypoint
    return new_start


if __name__ == "__main__":
    main()
