import json
import matplotlib.pyplot as plt
import math
from scipy.spatial import distance


class WayPoint:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.turn_next = []
        self.next = []
        self.before = []

    def find_next(self, starting_points, end_points, Points):
        next_points_xy = []
        next_points_class_idx = []
        for i, e in enumerate(starting_points):
            if e[0] == self.x:
                if e[1] == self.y:
                    next_points_xy.append(end_points[i])
        next_points_xy = [list(t) for t in set(tuple(element) for element in next_points_xy)]
        if next_points_xy:
            for next_point in next_points_xy:
                for i, Point in enumerate(Points):
                    if Point.x == next_point[0]:
                        if Point.y == next_point[1]:
                            next_points_class_idx.append(i)
        self.turn_next = [False for _ in range(len(next_points_class_idx))]
        return next_points_class_idx


def open_data(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data


def count_angle(current_xy, next_xy):
    myradians = math.atan2(next_xy[1] - current_xy[1], next_xy[0] - current_xy[0])
    return math.degrees(myradians)


if __name__ == '__main__':
    Points = []

    points = open_data('waypoints.txt')
    starting_points = [p[1] for p in points]
    end_points = [p[0] for p in points]

    points_noduplicates = starting_points.copy()
    points_noduplicates.extend([x for x in end_points if x not in starting_points])
    points_noduplicates = [list(t) for t in set(tuple(element) for element in points_noduplicates)]

    for point in points_noduplicates:
        Points.append(WayPoint(point[0], point[1]))

    for it, point in enumerate(Points):
        point.next = point.find_next(starting_points, end_points, Points)
        for next in point.next:
            Points[next].before.append(it)

    for point in Points:
        if len(point.next) > 0:
            print(f'obecny punkt: {point.x, point.y}')
            for i in range(len(point.next)):
                print(f'wspolrzedne nastepnego punktu {Points[point.next[i]].x, Points[point.next[i]].y}')
                print(f'indeks nastepnych punktow dla danego nastepnego punktu: {Points[point.next[i]].next}')
                # angle from one of the points that could be before this point to this point
                if point.before:
                    angle_0 = count_angle([Points[point.before[0]].x, Points[point.before[0]].y], [point.x, point.y])
                else:
                    angle_0 = 0
                euc_dist_0 = distance.euclidean([point.x, point.y], [Points[point.next[i]].x, Points[point.next[i]].y])
                for n in Points[point.next[i]].next:
                    euc_dist = distance.euclidean([point.x, point.y], [Points[n].x, Points[n].y])
                    angle = abs(angle_0 - count_angle([point.x, point.y], [Points[n].x, Points[n].y]))
                    difx = abs(Points[n].x - point.x)
                    dify = abs(Points[n].y - point.y)
                    print(f'wspolrzedne nastepnego punktu dla danego nastepnego punktu: {Points[n].x, Points[n].y}')
                    print(f'    kat: {angle}')
                    print(f'    odleglosc: {euc_dist}')
                    if len(point.next) > 1:
                        # if euc_dist > 15 and abs(angle) > 10:
                        #     point.turn_next[i] = True
                        if difx > 3.5 and dify > 3.5:
                            point.turn_next[i] = True
                    else:
                        if euc_dist > 15 and abs(angle) > 20 and 7 < euc_dist_0 < 15 and difx > 3.5 and dify > 3.5:
                            point.turn_next[i] = True
            print('\n')

    for val in points:
        x = [val[0][0], val[1][0]]
        y = [val[0][1], val[1][1]]
        plt.plot(x, y, 'g')

    for point in Points:
        for i, value in enumerate(point.turn_next):
            if value is True:
                x = [point.x, Points[point.next[i]].x]
                y = [point.y, Points[point.next[i]].y]
                plt.plot(x, y, 'r')

    # print(len(points))
    plt.scatter([p[0] for p in starting_points], [p[1] for p in starting_points], s=0.3)
    plt.show()
