"""
Env:
Windows 7
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
"""
import carla
import dataclasses
import math
import time
from carla import Transform, Location, Rotation
from loguru import logger


@dataclasses.dataclass
class Guide:
    _start_point: carla.libcarla.Waypoint
    _end_point: carla.libcarla.Waypoint
    _route = []

    def _log(self):
        logger.info(f'Start point: {self._start_point}')
        logger.info(f'End point: {self._end_point}')

    def guide_a_star(self, waypoints_range=20, all_waypoints=None):
        self._route = []
        self._log()
        start_time = time.time()
        start_waypoint = self._start_point
        end_waypoint = self._end_point

        open_list = []
        closed_list = []
        children = []

        open_list.append(start_waypoint)

        # Robię to dictami, bo trzeba jakoś przywiązać wartośći F, G i parent do każdego waypointa, a one mają ID
        G = {}  # Actual movement cost to each position from the start position
        F = {}  # Estimated movement cost of start to end going via this position {waypoint_id: f}
        cameFrom = {}  # Parent of a node in a form of { current_id: parent_waypoint}

        G[start_waypoint.id] = 0
        F[start_waypoint.id] = self.heuristic_distance_a_star(start_waypoint, end_waypoint)

        while open_list:
            lowest_f = math.inf
            lowest_openindx = 0
            # find waypoint in open list with lowest f
            for open_id, open_waypoint in enumerate(open_list):
                if open_waypoint.id in F:
                    if F[open_waypoint.id] < lowest_f:
                        lowest_f = F[open_waypoint.id]
                        lowest_openindx = open_id

            current_waypoint = open_list[lowest_openindx]

            # if current_waypoint.id == end_waypoint.id:
            if self.heuristic_distance_a_star(current_waypoint, end_waypoint) <= waypoints_range + 5:
                # Retrace route from end
                self._route = [current_waypoint]
                while current_waypoint.id in cameFrom:
                    current_waypoint = cameFrom[current_waypoint.id]
                    self._route.append(current_waypoint)
                self._route.reverse()
                logger.info(f'Route computed in {time.time() - start_time} [s]')

                return self._route  # Done

            open_list.remove(current_waypoint)
            closed_list.append(current_waypoint)

            # Find children/neighbours (can search current.next() instead)
            for next_waypoint in all_waypoints:
                if self.heuristic_distance_a_star(current_waypoint, next_waypoint) <= waypoints_range + 5:
                    children.append(next_waypoint)

            for child in children:
                if child in closed_list:  # allowed?
                    continue
                candidateG = G[current_waypoint.id] + self.heuristic_distance_a_star(current_waypoint, child)

                if child not in open_list:
                    open_list.append(child)
                elif candidateG >= G[child.id]:
                    continue

                cameFrom[child.id] = current_waypoint
                G[child.id] = candidateG
                H = self.heuristic_distance_a_star(child, end_waypoint)
                F[child.id] = candidateG + H

            children.clear()

    def guide_heuristic(self, waypoints_range=20):
        self._route = []
        self._log()
        start_time = time.time()
        while True:
            smallest_distance = math.inf
            new_start = None
            for waypoint in self._start_point.next(waypoints_range):
                distance = self.heuristic_distance(waypoint)
                if distance < smallest_distance:
                    smallest_distance = distance
                    new_start = waypoint
            self._route.append(new_start)
            self._start_point = new_start
            if smallest_distance < waypoints_range + 10:
                break
        logger.info(f'Route computed in {time.time() - start_time} [s]')
        return self._route

    def guide_most_forward_route(self, all_waypoints, waypoints_range, finish_approximation):
        start_time = time.time()
        start_waypoint = self._start_point
        end_waypoint = self._end_point
        self._route = [start_waypoint]
        curr_waypoint = start_waypoint
        while True:
            # Find waypoints that are near chosen waypoint
            surrounding_waypoinst = [wp for wp in all_waypoints if
                                     self.heuristic_distance_a_star(curr_waypoint, wp) <= 2.0 * waypoints_range]
            possible_next_waypoints = curr_waypoint.next(2.0 * waypoints_range)

            if len(possible_next_waypoints) > 1:    # It there is crossroad
                # Check if going forward is kind of optimal
                front_wp = self.check_is_front_available(possible_next_waypoints, end_waypoint, curr_waypoint,
                                                         waypoints_range, finish_approximation)

                if front_wp is not None:    # It it is OK to go forward
                    must_go_wp = front_wp.next(1.2 * waypoints_range)[0]
                    # Go forward 3 waypoints generated with consecutive next() to prevent algorithm from choosing waypoint behind the car
                    for i in range(3):
                        self._route.append(must_go_wp)
                        if self.heuristic_distance_a_star(end_waypoint, must_go_wp) < waypoints_range + finish_approximation:   # Reached end point
                            break
                        must_go_wp = must_go_wp.next(1.2 * waypoints_range)[0]
                    curr_waypoint = must_go_wp
                    if self.heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:    # Reached end point
                        break
                # If going forward makes car move away from end point, choose waypoint that is the nearest to end point in euclidean distance
                else:
                    new_curr_waypoint = self.find_closest_wp_to_end(surrounding_waypoinst, end_waypoint)
                    self._route.append(new_curr_waypoint)
                    curr_waypoint = new_curr_waypoint
                    if self.heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:    # Reached end point
                        break
            else:   # Else choose waypoint that is the nearest to end point in euclidean distance
                new_curr_waypoint = self.find_closest_wp_to_end(surrounding_waypoinst, end_waypoint)
                self._route.append(new_curr_waypoint)
                curr_waypoint = new_curr_waypoint
                if self.heuristic_distance_a_star(end_waypoint, curr_waypoint) < waypoints_range + finish_approximation:    # Reached end point
                    break
        logger.info(f'Route computed in {time.time() - start_time} [s]')
        return self._route

    def guide_shortest_euclidean_distance(self, waypoints_range=20, all_waypoints=None):
        self._route = []
        self._log()
        start_time = time.time()
        smallest_distance = math.inf
        while True:
            children = []
            for next_waypoint in all_waypoints:
                if self.heuristic_distance_a_star(self._start_point, next_waypoint) <= waypoints_range + 5:
                    children.append(next_waypoint)
            for waypoint in children:
                distance = self.heuristic_distance(waypoint)
                if distance < smallest_distance:
                    smallest_distance = distance
                    new_start = waypoint
            self._route.append(new_start)
            self._start_point = new_start
            if smallest_distance < waypoints_range + 5:
                break
        logger.info(f'Route computed in {time.time() - start_time} [s]')
        return self._route

    def guide_least_turns(self, waypoints_range=20):
        self._route = []
        self._log()
        start_time = time.time()
        self.original_start = self._start_point
        turn_count = 0
        while True:
            smallest_distance = math.inf
            new_start = None
            close = self._start_point.next(waypoints_range)
            for waypoint in self._start_point.next(waypoints_range):
                distance = self.heuristic_distance(waypoint)
                if distance < smallest_distance:
                    smallest_distance = distance
                    new_start = waypoint
            self._route.append(new_start)
            if len(self._route) > 2 and len(close) > 1:
                if self.turn_detection(self._route):
                    turn_count += 1
            self._start_point = new_start
            if smallest_distance < waypoints_range + 5:
                break
        logger.info(f'Route computed in {time.time() - start_time} [s]')
        self.turn_finder(turn_count, waypoints_range)
        return self._route

    def turn_detection(self, waypoints):
        x1 = abs(waypoints[-2].transform.location.x - waypoints[-1].transform.location.x)
        y1 = abs(waypoints[-2].transform.location.y - waypoints[-1].transform.location.y)
        x2 = abs(waypoints[-3].transform.location.x - waypoints[-2].transform.location.x)
        y2 = abs(waypoints[-3].transform.location.y - waypoints[-2].transform.location.y)
        if x1 + 5 <= x2 <= x1 - 5 and y1 + 5 <= y2 <= y1 - 5:
            return False
        return True

    def turn_finder(self, least_turns, waypoints_range):
        smallest_distance = math.inf
        new_start = None
        turn_list = []
        current_turns = []
        route = []
        turn_count = 0
        self._start_point = self.original_start
        while True:
            close = self._start_point.next(waypoints_range)
            distance = self.heuristic_distance(close[0])
            new_start = close[0]
            route.append(new_start)
            turn_list.append(0)
            if len(route) > 2 and len(close) > 1:
                if self.turn_detection(route):
                    turn_count += 1
                    current_turns.append(1)
                else:
                    current_turns.append(0)
            else:
                current_turns.append(0)
            self._start_point = new_start
            if smallest_distance < waypoints_range + 5:
                break
            if turn_count >= least_turns:
                while len(route[-1].next(waypoints_range)) < turn_list[-1]+2:
                    close = route[-1].next(waypoints_range)
                    route.pop()
                    turn_list.pop()
                    if current_turns[-1] == 1:
                        turn_count -= 1
                    current_turns.pop()
                close = route[-1].next(waypoints_range)
                route.append(route[-1].next(waypoints_range)[turn_list[-1] + 1])
                turn_list[-1] += 1
                if len(route) > 2 and len(close) > 1:
                    if self.turn_detection(route):
                        turn_count += 1
                        current_turns.append(1)
                    else:
                        current_turns.append(0)
                else:
                    current_turns.append(0)
        logger.info(f'Route computed in {time.time() - start_time} [s]')
        new_guide = route

    def guide_dijkstra(self, waypoints_range=20, all_waypoints=None):
        self._route = []
        self._log()
        start_time = time.time()
        start = self._start_point
        end = self._end_point
        open_list, closed_list, children, F, G, cameFrom = [], [], [], {}, {}, {}
        open_list.append(start)
        G[start.id] = 0
        F[start.id] = self.heuristic_distance_a_star(start, end)

        while open_list:
            lowest_f = math.inf
            lowest_openindx = 0
            for open_id, open_waypoint in enumerate(open_list):
                if open_waypoint.id in F:
                    if F[open_waypoint.id] < lowest_f:
                        lowest_f = F[open_waypoint.id]
                        lowest_openindx = open_id
            current = open_list[lowest_openindx]

            if self.heuristic_distance_a_star(current, end) <= waypoints_range + 5:
                self._route = [current]
                while current.id in cameFrom:
                    current = cameFrom[current.id]
                    self._route.append(current)
                self._route.reverse()
                logger.info(f'Route computed in {time.time() - start_time} [s]')
                return self._route

            open_list.remove(current)
            closed_list.append(current)

            for next in all_waypoints:
                if self.heuristic_distance_a_star(current, next) <= waypoints_range + 5:
                    children.append(next)

            for x in children:
                if x in closed_list:
                    continue
                candidate = G[current.id] + self.heuristic_distance_a_star(current, x)
                if x not in open_list:
                    open_list.append(x)
                elif candidate >= G[x.id]:
                    continue

                cameFrom[x.id] = current
                G[x.id] = candidate
                F[x.id] = candidate

            children.clear()

    def heuristic_distance(self, waypoint):
        x_distance = waypoint.transform.location.x - self._end_point.transform.location.x
        y_distance = waypoint.transform.location.y - self._end_point.transform.location.y
        return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))

    @staticmethod
    def heuristic_distance_a_star(waypoint1, waypoint2):
        # Distance between two waypoints
        x_distance = waypoint1.transform.location.x - waypoint2.transform.location.x
        y_distance = waypoint1.transform.location.y - waypoint2.transform.location.y
        return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))

    def check_is_front_available(self, possible_next_waypoints, finish_waypoint, crossroad_waypoint, waypoints_range,
                                 finish_approximation):
        frontal_waypoint = None
        min_angle_dif = math.inf
        # Find waypoint with the proper value of yaw
        for next_wp in possible_next_waypoints:
            angle_dif = abs(next_wp.transform.rotation.yaw - crossroad_waypoint.transform.rotation.yaw)
            if angle_dif < 0.5 and angle_dif < min_angle_dif:
                frontal_waypoint = next_wp
                min_angle_dif = angle_dif

        if frontal_waypoint is not None:
            # Check if going with route pointed by frontal waypoint is favourable, if distance to end point will start
            # to grow then it is unfavourable
            old_distance = self.heuristic_distance_a_star(frontal_waypoint, finish_waypoint)
            available_wps_after_frontal = frontal_waypoint.next(1.2 * waypoints_range)
            while len(available_wps_after_frontal) == 1:    # Generate route to the next crossroad
                new_frontal_waypoint = available_wps_after_frontal[0]
                new_distance = self.heuristic_distance_a_star(new_frontal_waypoint, finish_waypoint)
                if new_distance < waypoints_range + finish_approximation:   # Reached end point
                    return frontal_waypoint
                elif new_distance < old_distance:
                    old_distance = new_distance
                    available_wps_after_frontal = new_frontal_waypoint.next(1.2 * waypoints_range)
                    continue
                else:
                    return None
            return frontal_waypoint
        else:
            return None

    def find_closest_wp_to_end(self, surrounding_waypoinst, end_waypoint):
        """
        Find waypoint closest to end point from surrounding waypoints
        :param surrounding_waypoinst: list of surrounding waypoints
        :param end_waypoint: end waypoint
        :return: waypoint the nearest to end point in euclidean distance
        """
        smallest_distance = math.inf
        new_start = None
        for waypoint in surrounding_waypoinst:
            distance = self.heuristic_distance_a_star(end_waypoint, waypoint)
            if distance < smallest_distance:
                smallest_distance = distance
                new_start = waypoint
        return new_start
