"""
Env:
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
"""

import carla
from loguru import logger as log

from src import consts
from src.util.parse_arguments import parse_arguments
from src.colision_detection.collision_avoidance import CollisionDetector
from src.navigation.automatic_control import navigation_loop
from src.tools.json_parser import ConfigParser
from src.traffic_light.brake_control_module.brake_control import BrakeControl
from src.traffic_light.create_light import create_race_start_light

world_actors_list = []


def main():
    cam_width = 1600
    cam_height = 900
    sensor_tick = 0.1
    start_from_light = True

    # initialization
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.START_CONFIG_DIR)

    args = parse_arguments()
    throttle = args.throttle
    steer = args.steer
    frames = args.frames
    time = args.time

    world = client.get_world()
    map_ = world.get_map()
    blueprint_library = world.get_blueprint_library()
    spawn_points_list = map_.get_spawn_points()
    spawn = spawn_points_list[args.vehicle_start]
    destination = spawn_points_list[args.vehicle_destination]

    # creating vehicle
    car_bp = blueprint_library.filter("model3")[0]
    vehicle = world.spawn_actor(car_bp, spawn)
    world_actors_list.append(vehicle)

    # connecting collision detection system
    anti_collision_sys = CollisionDetector(world, vehicle, throttle, steer, frames, time)
    anti_collision_sys.run()
    world_actors_list.append(anti_collision_sys)

    # Creating RGB Camera
    rgb_camera_bp = blueprint_library.find('sensor.camera.rgb')
    rgb_camera_bp.set_attribute('image_size_x', str(cam_width))
    rgb_camera_bp.set_attribute('image_size_y', str(cam_height))
    rgb_camera_bp.set_attribute('fov', '110')
    rgb_camera_bp.set_attribute('sensor_tick', str(sensor_tick))
    cam_transform = carla.Transform(carla.Location(x=0.8, z=1.7), carla.Rotation(yaw=0, pitch=7))
    rgb_camera = world.spawn_actor(rgb_camera_bp, cam_transform, attach_to=vehicle)
    world_actors_list.append(rgb_camera)

    # Creating traffic light detection system
    traffic_light_detection_system = BrakeControl(config_dict=config_parser.config, time_wait_after_red=0.5,
                                                  rgb_cam_actor=rgb_camera, calculating_method="fuzzy")

    if start_from_light:
        create_race_start_light(reference_vehicle_transform=vehicle.get_transform(), world=world, change_after=8)

    # running control
    navigation_loop(client=client, config_dict=config_parser.config, actor=vehicle, destination=destination,
                    anti_collision_sys=anti_collision_sys, logs=True,
                    traffic_light_detection_system=traffic_light_detection_system)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        log.error(f'Some exception occured: {e}')
    finally:
        for actor in world_actors_list:
            actor.destroy()
