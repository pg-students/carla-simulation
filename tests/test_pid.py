import numpy as np
import time


class PID(object):
    """PID Controller
    """

    def __init__(self, P=0.2, I=0.0, D=0.0):

        self.Kp = P
        self.Ki = I
        self.Kd = D

        self.sample_time = 0.00
        self.current_time = time.time()
        self.last_time = self.current_time

        self.clear()

    def clear(self):
        """Czyści obliczenia i współczynniki PID"""
        self.PTerm = 0.0
        self.ITerm = 0.0
        self.DTerm = 0.0
        self.last_error = 0.0

        # Windup Guard
        self.int_error = 0.0
        self.windup_guard = 20.0

        self.output = 0.0

    def update(self, error, output_limits=[-1.0, 1.0]):
        """ Oblicza wartość PID dla danego sprzężenia zwrotnego
        .. Równanie
            u (t) = K_p e (t) + K_i \ int_ {0} ^ {t} e (t) dt + K_d {de} / {dt}
        """

        self.current_time = time.time()
        delta_time = self.current_time - self.last_time
        delta_error = error - self.last_error

        if (delta_time >= self.sample_time):
            self.PTerm = self.Kp * error
            self.ITerm += error * delta_time

            if (self.ITerm < -self.windup_guard):
                self.ITerm = -self.windup_guard
            elif (self.ITerm > self.windup_guard):
                self.ITerm = self.windup_guard

            self.DTerm = 0.0
            if delta_time > 0:
                self.DTerm = delta_error / delta_time

            # Zapamiętaj ostatni raz i ostatni błąd do następnych obliczeń
            self.last_time = self.current_time
            self.last_error = error

            self.output = self.PTerm + (self.Ki * self.ITerm) + (self.Kd * self.DTerm)
            self.output = np.clip(self.output, output_limits[0], output_limits[1])

    def setKp(self, proportional_gain):
        """Określa, jak agresywnie PID reaguje na bieżący błąd po ustawieniu wzmocnienia proporcjonalnego"""
        self.Kp = proportional_gain

    def setKi(self, integral_gain):
        """Określa, jak agresywnie PID reaguje na bieżący błąd po ustawieniu wzmocnienia całkowania"""
        self.Ki = integral_gain

    def setKd(self, derivative_gain):
        """Określa, jak agresywnie PID reaguje na bieżący błąd po ustawieniu wzmocnienia pochodnego"""
        self.Kd = derivative_gain

    def setWindup(self, windup):
        """Zintegrowana likwidacja, znana również jako integracja lub resetowanie,
        odnosi się do sytuacji w regulatorze sprzężenia zwrotnego PID, gdzie
        występuje duża zmiana wartości zadanej
        a warunki całkowite kumulują znaczny błąd
        podczas wzlotu (windup), w ten sposób przekraczając i kontynuując
        wzrost w miarę rozwijania się tego nagromadzonego błędu
        (kompensowane przez błędy w innym kierunku).
        Konkretnym problemem jest przekroczenie limitu.
        """
        self.windup_guard = windup

    def setSampleTime(self, sample_time):
        """PID, który powinien być aktualizowany w regularnych odstępach czasu.
        Na podstawie wcześniej określonego czasu próbkowania PID decyduje, czy powinien natychmiast obliczyć, czy powrócić.
        """
        self.sample_time = sample_time