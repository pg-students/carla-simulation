import carla
import time
from carla import Transform, Location, Rotation
from loguru import logger as log

from src import consts
from src.tools.json_parser import ConfigParser
from src.traffic_light.brake_control_module.brake_control import BrakeControl

cam_width = 1600
cam_height = 900
sensor_tick = 0.3


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    transform = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    car_bp = blueprint_library.filter("vehicle.tesla.*")[0]

    car_actor = world.spawn_actor(car_bp, transform)

    # Find the blueprint of the sensor.
    rgb_bp = blueprint_library.find('sensor.camera.rgb')
    # Modify the attributes of the blueprint to set image resolution and field of view.
    rgb_bp.set_attribute('image_size_x', str(cam_width))
    rgb_bp.set_attribute('image_size_y', str(cam_height))
    rgb_bp.set_attribute('fov', '110')
    # Set the time in seconds between sensor captures
    rgb_bp.set_attribute('sensor_tick', str(sensor_tick))
    # Provide the position of the sensor relative to the vehicle.
    transform = carla.Transform(carla.Location(x=0.8, z=1.7), Rotation(yaw=0, pitch=7))
    # Tell the world to spawn the sensor, don't forget to attach it to your vehicle actor.
    sensor = world.spawn_actor(rgb_bp, transform, attach_to=car_actor)

    brake_control = BrakeControl(config_dict=config_parser.config, time_wait_after_red=2, rgb_cam_actor=sensor,
                                 calculating_method="fuzzy")

    log.info("Autopilot ON")
    car_actor.set_autopilot(True)

    duration = 10000
    time_end = time.time() + duration

    while time.time() < time_end:
        brake_value = brake_control.get_brake_value()
        log.info(f"brake value: {brake_value}")
        if brake_value > 0:
            car_actor.set_autopilot(False)
            curr_control = car_actor.get_control()
            curr_control.brake = 1
            car_actor.apply_control(curr_control)
        else:
            car_actor.set_autopilot(True)

        time.sleep(sensor_tick)

    sensor.destroy()
    car_actor.destroy()
    log.info("Detection interrupted")


if __name__ == "__main__":
    main()
