import heapq
import carla

class Cell(object):
    def __init__(self, x, y, reachable):
        """Zainicjuj nową komórkę.
         @param osiągalny czy komórka jest osiągalna? nie ściana?
         @param x komórka x współrzędna x
         @param y współrzędna y komórki
         @param g koszt przejścia z komórki początkowej do tej komórki.
         @param oszacowanie kosztu przejścia z tej komórki
                  do końcowej komórki.
         @param f f = g + h
        """
        _start_point: carla.libcarla.Waypoint
        _end_point: carla.libcarla.Waypoint

        self.reachable = reachable
        self.x = x
        self.y = y
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0

    def __lt__(self, other):
        return self.g < other.g


class AStar(object):
    def __init__(self):
        # otwarta lista
        self.opened = []
        heapq.heapify(self.opened)
        # lista odwiedzonych komorek
        self.closed = set()
        self.cells = []
        self.grid_height = None
        self.grid_width = None
        self.start = None
        self.end = None

    def init_grid(self, width, height, walls, start, end):
        """Przygotowanie komórki siatki, ściany.
         @param width szerokość siatki.
         @param height wysokość siatki.
        """
        self.grid_height = height
        self.grid_width = width
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                if (x, y) in walls:
                    reachable = False
                else:
                    reachable = True
                self.cells.append(Cell(x, y, reachable))
        self.start = self.get_cell(*start)
        self.end = self.get_cell(*end)

    def get_heuristic(self, cell):
        """Oblicz wartość heurystyczną H dla komórki.
         Odległość między tą komórką a komórką końcową pomnożona przez 10.
         @ zwraca wartość heurystyczną H.
        """
        return 10 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y))

    def get_cell(self, x, y):
        """Zwraca komórkę z listy komórek.
         @param x komórka x współrzędna x
         @param y współrzędna y komórki
         @zwraca komorke
        """
        return self.cells[x * self.grid_height + y]

    def get_adjacent_cells(self, cell):
        """Zwraca sąsiednie komórki do komórki.
         Zgodnie z ruchem wskazówek zegara, zaczynając od tego po prawej.
         Komórka @param pobiera sąsiednie komórki dla tej komórki
         @ zwraca listę sąsiednich komórek.
        """
        cells = []
        if cell.x < self.grid_width - 1:
            cells.append(self.get_cell(cell.x + 1, cell.y))
        if cell.y > 0:
            cells.append(self.get_cell(cell.x, cell.y - 1))
        if cell.x > 0:
            cells.append(self.get_cell(cell.x - 1, cell.y))
        if cell.y < self.grid_height - 1:
            cells.append(self.get_cell(cell.x, cell.y + 1))
        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y)]
        while cell.parent is not self.start:
            cell = cell.parent
            path.append((cell.x, cell.y))

        path.append((self.start.x, self.start.y))
        path.reverse()
        return path

    def update_cell(self, adj, cell):
        """Zaktualizuj sąsiednią komórkę.
         @param przylegająca komórka do bieżącej komórki
         Przetwarzana komórka prądowa @param
        """
        adj.g = cell.g + 10
        adj.h = self.get_heuristic(adj)
        adj.parent = cell
        adj.f = adj.h + adj.g

    def solve(self, printing_grid):
        """Rozwiąż labirynt, znajdź ścieżkę do końcowej komórki.
         @zwraca sciezke lub None, jeśli nie znaleziono.
        """
        # dodaj komórkę początkową, aby otworzyć kolejkę sterty
        heapq.heappush(self.opened, (self.start.f, self.start))
        printing_grid[self.start.x, self.start.y] = 2.0
        printing_grid[self.end.x, self.end.y] = 4.0

        while len(self.opened):
            # pop komórka z kolejki sterty
            _, cell = heapq.heappop(self.opened)

            # dodaj komórkę do zamkniętej listy, aby nie przetwarzać jej dwukrotnie
            printing_grid[cell.x, cell.y] = 3.0

            self.closed.add(cell)
            # jeśli komórka końcowa, zwróć znalezioną ścieżkę
            if cell is self.end:
                return self.get_path()
            # uzyskać sąsiednie komórki dla komórki
            adj_cells = self.get_adjacent_cells(cell)
            for adj_cell in adj_cells:
                if adj_cell.reachable and adj_cell not in self.closed:
                    if (adj_cell.f, adj_cell) in self.opened:
                        # jeśli sąsiednia komórka na otwartej liście, sprawdź, czy jest obecna ścieżka
                        # lepszy niż poprzednio znaleziony
                        # dla tej przyległej komórki.
                        if adj_cell.g > cell.g + 10:
                            self.update_cell(adj_cell, cell)
                    else:
                        self.update_cell(adj_cell, cell)
                        # dodaj sąsiednią komórkę, aby otworzyć listę
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))