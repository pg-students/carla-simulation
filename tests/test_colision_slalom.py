import glob
import os
import sys
import random
import time
import numpy as np
import math

from src.colision_detection.collision_avoidance import CollisionDetector

import carla

world_actors_list = []


def main():
    client = carla.Client("localhost", 2000)
    client.set_timeout(10.0)
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()
    model3_transform = carla.Transform(carla.Location(x=-13.5, y=100, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model3_blueprint = blueprint_library.filter("model3")[0]
    model3 = world.spawn_actor(model3_blueprint, model3_transform)
    model32_transform = carla.Transform(carla.Location(x=-5.5, y=130, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model33_transform = carla.Transform(carla.Location(x=-13.5, y=130, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model34_transform = carla.Transform(carla.Location(x=-5.5, y=138, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model35_transform = carla.Transform(carla.Location(x=-13.5, y=143, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model36_transform = carla.Transform(carla.Location(x=-5.5, y=147, z=1.6431), carla.Rotation(pitch=0, yaw=90, roll=0))
    model32_blueprint = blueprint_library.filter("model3")[0]
    model32 = world.spawn_actor(model32_blueprint, model32_transform)
    model33 = world.spawn_actor(model32_blueprint, model33_transform)
    model34 = world.spawn_actor(model32_blueprint, model34_transform)
    model35 = world.spawn_actor(model32_blueprint, model35_transform)
    model36 = world.spawn_actor(model32_blueprint, model36_transform)
    model3_colision_detector = CollisionDetector(world, model3, is_auto=True)
    world_actors_list.append(model3_colision_detector)
    world_actors_list.append(model3)
    world_actors_list.append(model32)
    world_actors_list.append(model33)
    world_actors_list.append(model34)
    world_actors_list.append(model35)
    world_actors_list.append(model36)
    model3_colision_detector.run()
    while True:
        pass


if __name__ == '__main__':
    try:
        main()

    finally:
        for actor in world_actors_list:
            actor.destroy()
