"""
Env:
Windows 10
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
Python libs: numpy==1.17.3, pygame==1.9.6
"""

import carla
from carla import Transform, Location, Rotation
from time import sleep


def main():

    # --- Client creation ---
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds

    actors_to_destroy_list = []

    # --- Retriveing world ---
    world = client.get_world()

    # --- Map change ---
    # world = client.load_world('Town03')
    # world = client.reload_world()

    # --- Retrieving loaded map ---
    map = world.get_map()

    # --- Retrieving blueprints ---
    blueprint_library = world.get_blueprint_library()

    # --- All vehicles ---
    vehicles = blueprint_library.filter('vehicle.*')
    # for v in vehicles:
    #     print(v)

    # --- All recomended spawn points on map ---
    spawn_points = map.get_spawn_points()
    # for s in spawn_points:
    #     print(s)

    # --- Maps --- 
    # --- (7 maps available ) ---
    print(client.get_available_maps())

    # --- Specific car bluprint
    tesla_blueprint = blueprint_library.filter('vehicle.tesla.model3')[0]
    mercedes_blueprint = blueprint_library.filter('vehicle.mercedes-benz.coupe')[0]

    # --- Vechicle color change ---
    tesla_blueprint.set_attribute('color', '0,0,0')
    mercedes_blueprint.set_attribute('color', '255,0,0')

    # --- Select spawn point by hand ---
    # tesla_transform = Transform(Location(x=-85.5, y=-150, z=1.843),  Rotation(pitch=0, yaw=90, roll=0))
    tesla_transform = Transform(Location(x=-89, y=-135, z=1.843),  Rotation(pitch=0, yaw=90, roll=0))
    mercedes_transform = Transform(Location(x=-89, y=-150, z=1.843),  Rotation(pitch=0, yaw=90, roll=0))

    # --- spawn vechicle ---
    tesla = world.spawn_actor(tesla_blueprint, tesla_transform)
    actors_to_destroy_list.append(tesla)

    mercedes = world.spawn_actor(mercedes_blueprint, mercedes_transform)
    actors_to_destroy_list.append(mercedes)

    sleep(3)

    # --- Add obstacle sensor to vechicle ---
    # sensor = obstacle_sensor(mercedes, world)
    # actors_to_destroy_list.append(sensor)

    # --- Drive control (straight ahead) ---
    # tesla.apply_control(carla.VehicleControl(throttle=1.0, steer=-0.002))
    # mercedes.apply_control(carla.VehicleControl(throttle=1.0, steer=-0.001))

    # sleep(10)

    # --- Drive control break) ---
    # tesla.apply_control(carla.VehicleControl(brake=1.0))
    # mercedes.apply_control(carla.VehicleControl(brake=1.0))

    # sleep(5)

    # --- Turn on autopilot ---
    # tesla.set_autopilot(True)
    # mercedes.set_autopilot(True)

    # --- Add camera to vichicle (with image save in output folder) ---
    # camera = add_camera(mercedes, world)
    # actors_to_destroy_list.append(camera)

    # sleep(5)

    # --- Add lidar to vichicle (with image save in output folder) ---
    # abs_sensor = absolute_stop_sensor(mercedes, world)
    # actors_to_destroy_list.append(abs_sensor)

    # --- Getting vechicle waypoint ---
    # mercedes_waypoint = get_waypoint(mercedes, map)
    # sleep(5)

    # --- Print line types around vechicle ---
    # line_types(mercedes_waypoint)

    # --- Destroying all objects ---
    for actor in actors_to_destroy_list:
        actor.destroy()


def absolute_stop_sensor(car, world):
    sensor_blueprint = world.get_blueprint_library().find('sensor.other.obstacle')
    sensor_blueprint.set_attribute('debug_linetrace', 'true')
    sensor_blueprint.set_attribute('sensor_tick', '0.1')
    transform = carla.Transform(carla.Location())
    sensor = world.spawn_actor(sensor_blueprint, transform, attach_to=car)
    sensor.listen(lambda data:  absolute_stop(car, data))
    return sensor


def absolute_stop(car, data):
    car.apply_control(carla.VehicleControl(brake=1.0, steer=-0.0))
    print(data)


def obstacle_sensor(car, world):
    sensor_blueprint = world.get_blueprint_library().find('sensor.other.obstacle')
    sensor_blueprint.set_attribute('debug_linetrace', 'true')
    sensor_blueprint.set_attribute('sensor_tick', '0.1')
    transform = carla.Transform(carla.Location())
    sensor = world.spawn_actor(sensor_blueprint, transform, attach_to=car)
    sensor.listen(lambda data: do_sensor(data))
    return sensor


def do_sensor(data):
    print(data)


def get_waypoint(car, map):
    waypoint = map.get_waypoint(car.get_location(), project_to_road=True,
                                lane_type=(carla.LaneType.Driving | carla.LaneType.Sidewalk))
    return waypoint


def line_types(waypoint):
    print(waypoint.lane_type)
    print(waypoint.lane_change)
    print(waypoint.right_lane_marking.type)


def add_lidar(car, world):
    relative_transform = Transform(Location(x=0, y=2, z=1), Rotation(pitch=0, yaw=0, roll=0))
    lidar_blueprint = world.get_blueprint_library().find('sensor.lidar.ray_cast')
    lidar_blueprint.set_attribute('range', '15000')
    # lidar_blueprint.set_attribute('upper_fov', '15')
    # lidar_blueprint.set_attribute('lower_fov', '-45')
    # lidar_blueprint.set_attribute('points_per_second', '15000')
    # lidar_blueprint.set_attribute('rotation_frequency', '15')
    lidar = world.spawn_actor(lidar_blueprint, relative_transform, attach_to=car)
    lidar.listen(lambda image: image.save_to_disk('output/%06d.txt' % image.frame_number))
    return lidar


def add_camera(car, world):
    relative_transform = Transform(Location(x=-7, y=0, z=2.5), Rotation(pitch=0, yaw=0, roll=0))
    camera_blueprint = world.get_blueprint_library().find('sensor.camera.rgb')
    camera = world.spawn_actor(camera_blueprint, relative_transform, attach_to=car)
    camera.listen(lambda image: image.save_to_disk('output/%06d.png' % image.frame_number))
    return camera


#
#   Tested functionality
#


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        print('\nProcess done.')
