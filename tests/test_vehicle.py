import carla
import time
from carla import Transform, Location, Rotation, command


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]

    transform = Transform(Location(x=30, y=5, z=1), Rotation(yaw=0))
    car_actor = world.spawn_actor(car_bp, transform)
    time.sleep(1)

    car_control = carla.VehicleControl()

    car_actor.apply_control(car_control)

    print("forward")
    car_control.throttle = 1    # Cannot set by reference, need to call apply_control
    car_actor.apply_control(car_control)
    for i in range(5):
        time.sleep(1)
        print("current gear: {}".format(car_actor.get_control().gear))

    print("turning")
    car_control.throttle = 1
    car_control.steer = -0.5  # -1 is turn left, +1 is turn right
    # To much steer (for example -1) blocks wheels
    car_actor.apply_control(car_control)
    # command.ApplyVehicleControl(car_actor, car_control)   # Doesn't work
    time.sleep(3)

    print("braking")
    car_control.throttle = 1
    car_control.brake = 0.1  # To little brake doesn't stop car
    car_actor.apply_control(car_control)
    time.sleep(3)

    print("reset")
    car_control.throttle = 0
    car_control.brake = 0.0  # To little brake doesn't stop car
    # car_actor.apply_control(car_control)
    command.ApplyVehicleControl(car_actor, car_control)
    time.sleep(3)
    print('Forward by reference')

    a = car_actor.get_control()

    # car_actor.get_control().throttle = 1 # Doesn't work
    # car_actor.apply_control(car_actor.get_control().__init__(throttle=1.0)) # Doesn't work too
    # Vehicle code is in C++ so it is not possible to access its private params from python, only apply_control
    time.sleep(5)
    car_actor.destroy()

    # car_actor = world.spawn_actor(car_bp, transform)
    # time.sleep(1)
    # print("Autopilot ON")
    # car_actor.set_autopilot(True)
    #
    # time.sleep(100)
    # car_actor.destroy()


if __name__ == "__main__":
    main()



