import carla
import time
from carla import Transform, Location, Rotation


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()

    actor_list = world.get_actors()
    # Find an actor by id.
    actor = actor_list.find(10)
    print(actor)
    # Print the location of all the speed limit signs in the world.
    for speed_sign in actor_list.filter('traffic.speed_limit.*'):
        print(speed_sign.get_location())

    weather = carla.WeatherParameters(
        cloudyness=80.0,
        precipitation=30.0,
        sun_altitude_angle=70.0)

    world.set_weather(weather)

    print(world.get_weather())

    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]

    transform = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    transform2 = Transform(Location(x=2.5, y=30, z=3), Rotation(yaw=-90))
    car_actor = world.spawn_actor(car_bp, transform2)
    # time.sleep(2)
    # car_actor.is_alive = False

    map = world.get_map()
    waypoint = map.get_waypoint(car_actor.get_location())

    print("Waypoint jumps")
    for i in range(40):
        time.sleep(0.5)
        waypoint = waypoint.next(1.0)[0]
        car_actor.set_transform(waypoint.transform)

    print("generate waypoints")

    waypoint_list = map.generate_waypoints(1.0)
    for i in range(10):
        time.sleep(0.5)
        car_actor.set_transform(waypoint_list[i].transform)

    car_actor.set_transform(waypoint.transform)
    # settings = world.get_settings()
    # settings.no_rendering_mode = True
    # world.apply_settings(settings)
    #
    # time.sleep(2)
    #
    # settings = world.get_settings()
    # settings.no_rendering_mode = False
    # world.apply_settings(settings)

    time.sleep(2)
    # Works until yaw is in quarter (-90, 0), for others there should be some ifs
    print("Waypoint drive")
    car_actor.set_transform(transform)
    car_control = carla.VehicleControl(throttle=0.4)
    car_actor.apply_control(car_control)
    for i in range(50):
        time.sleep(0.5)
        new_waypoint = map.get_waypoint(car_actor.get_location()).next(1.0)[0]
        print(new_waypoint)
        print(car_actor.get_transform().rotation.yaw)
        difference = abs(new_waypoint.transform.rotation.yaw) - abs(car_actor.get_transform().rotation.yaw)
        print("diff = {}".format(difference))
        if difference > 0:
            car_control.steer = -0.2
        else:
            car_control.steer = +0.2
        car_actor.apply_control(car_control)

    car_actor.destroy()
    print("End")


if __name__ == "__main__":
    main()
