import cv2
import matplotlib.pyplot as plt

from src import consts
from src.tools.json_parser import ConfigParser
from src.traffic_light.detector.detector import Detector

IMG_PATH = 'img.jpg'


def main():
    frame = cv2.imread(IMG_PATH)
    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    plt.imshow(frame)
    plt.show()

    # apply object detection
    detector = Detector(config_dict=config_parser.config)
    out = detector.get_img_with_detection(frame)
    plt.imshow(out)
    plt.show()
    cv2.imshow('out', out)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
