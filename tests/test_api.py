import carla
import random
import time
from carla import Transform, Location, Rotation


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)  # seconds
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()
    print(blueprint_library)
    # for bp in blueprint_library:
    #     print(bp)

    # Find specific blueprint.
    collision_sensor_bp = blueprint_library.find('sensor.other.collision')
    # Chose a vehicle blueprint at random.
    vehicle_bp = random.choice(blueprint_library.filter('vehicle.bmw.*'))

    vehicles = blueprint_library.filter('vehicle.*')

    single_vehicle = vehicles[0]
    # for attr in single_vehicle:
    #     print(attr)

    bikes = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 2]
    for bike in bikes:
        # for attr in bike:
        #     print(attr)
        if bike.has_attribute("color"):     # Parę rowerów nie ma parametru "color" mimo, że w dokumentacji mają
            bike.set_attribute('color', '255,0,0')

    blueprint = blueprint_library.filter("vehicle.yamaha.yzf")[0]
    for attr in blueprint:
        if attr.is_modifiable:
            blueprint.set_attribute(attr.id, random.choice(attr.recommended_values))

    print("end")

    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]

    actor_list = world.get_actors()
    me = actor_list.filter("spectator")[0]
    curr_light = random.choice(actor_list.filter("traffic.*"))
    # for actor in actor_list:
    #     print(actor)
    print("my location:")
    print(me.get_location())
    transform = Transform(Location(x=30, y=5, z=3), Rotation(yaw=0))
    actor = world.spawn_actor(car_bp, transform)
    time.sleep(2)
    curr_light.set_location(Location(x=30, y=5, z=10))
    curr_light.trigger_volume.location = Location(x=30, y=5, z=10)
    actor.set_location(Location(x=30, y=5, z=5))
    print("actor location:")
    location = actor.get_location()
    print(location)
    for i in range(5):
        location.x += 1
        actor.set_location(location)
        time.sleep(1)

    # location.z += 10.0
    # actor.set_location(location)
    car_vel = actor.get_velocity()
    car_acc = actor.get_acceleration()
    print(car_acc)
    print(car_vel)
    time.sleep(1)
    car_vel.x = 5
    actor.set_velocity(car_vel)
    time.sleep(5)
    car_vel.x = 0
    actor.set_velocity(car_vel)
    # time.sleep(1)
    # car_acc.x = 5
    # actor.set_acceleration(car_acc)   # Nie ma set_acceleration

    time.sleep(10)
    actor.destroy()


if __name__ == "__main__":
    main()
