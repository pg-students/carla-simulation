"""
Env:
Windows 10
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
Python libs: numpy==1.17.2, pygame==1.9.6
"""
import argparse
import carla
import logging
import random


def main():
    argparser = argparse.ArgumentParser(
        description=__doc__)
    argparser.add_argument(
        '--host',
        metavar='H',
        default='127.0.0.1',
        help='IP of the host server (default: 127.0.0.1)')
    argparser.add_argument(
        '-p', '--port',
        metavar='P',
        default=2000,
        type=int,
        help='TCP port to listen to (default: 2000)')
    argparser.add_argument(
        '-n', '--number-of-vehicles',
        metavar='N',
        default=8,
        type=int,
        help='number of vehicles (default: 8)')
    argparser.add_argument(
        '-d', '--delay',
        metavar='D',
        default=2.0,
        type=float,
        help='delay in seconds between spawns (default: 2.0)')
    argparser.add_argument(
        '--safe',
        action='store_true',
        help='avoid spawning vehicles prone to accidents')
    args = argparser.parse_args()

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

    actor_list = []
    client = carla.Client(args.host, args.port)
    client.set_timeout(2.0)

    try:

        world = client.get_world()
        blueprints = world.get_blueprint_library()

        if args.safe:
            blueprints = [x for x in blueprints if int(x.get_attribute('number_of_wheels')) == 4]
            blueprints = [x for x in blueprints if not x.id.endswith('isetta')]
            blueprints = [x for x in blueprints if not x.id.endswith('carlacola')]

        # @todo cannot import these directly.
        SpawnActor = carla.command.SpawnActor
        SetAutopilot = carla.command.SetAutopilot
        FutureActor = carla.command.FutureActor

        batch = []
        colors = ['0,0,0', '255,255,255', '128,128,128', '0,128,128', '0,0,255', '0,255,0', '255,0,0', '255,255,0',
                  '0,128,100', '100,198,20']
        x = 17

        for n in range(args.number_of_vehicles):
            if n%2 == 0:
                y = -193.6
            else:
                y = -190
            blueprint = random.choice(blueprints.filter('vehicle.audi.tt'))
            blueprint.set_attribute('color', colors[n])
            blueprint.set_attribute('role_name', 'autopilot')
            transform = carla.Transform(carla.Location(x=x, y=y, z=10), carla.Rotation(yaw=0))
            batch.append(SpawnActor(blueprint, transform).then(SetAutopilot(FutureActor, True)))
            x -= 5

        for response in client.apply_batch_sync(batch):
            if response.error:
                logging.error(response.error)
            else:
                actor_list.append(response.actor_id)

        print('spawned %d vehicles, press Ctrl+C to exit.' % len(actor_list))

        while True:
            world.wait_for_tick()

    finally:

        print('\ndestroying %d actors' % len(actor_list))
        client.apply_batch([carla.command.DestroyActor(x) for x in actor_list])


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        print('\ndone.')
