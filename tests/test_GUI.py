# -*- coding: utf-8 -*-
import time

from PyQt5 import QtCore, QtGui, QtWidgets
import math
import carla
import matplotlib.pyplot as plt
from src.base_routing.guide_class import Guide

finish_distance_approximation = 5


class Ui_Algorytmy(object):
    def setupUi(self, Algorytmy):
        Algorytmy.setObjectName("Algorytmy")
        Algorytmy.setEnabled(True)
        Algorytmy.setFixedSize(540, 370)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/carla_black.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Algorytmy.setWindowIcon(icon)
        Algorytmy.setAutoFillBackground(False)
        # self.CarlaLogo = QtWidgets.QLabel(Algorytmy)
        # self.CarlaLogo.setGeometry(QtCore.QRect(350, 10, 180, 135))
        # self.CarlaLogo.setText("")
        # self.CarlaLogo.setPixmap(QtGui.QPixmap("images/carla.jpg"))
        # self.CarlaLogo.setScaledContents(True)
        # self.CarlaLogo.setObjectName("CarlaLogo")
        self.pushButton_Run = QtWidgets.QPushButton(Algorytmy)
        self.pushButton_Run.setGeometry(QtCore.QRect(360, 10, 160, 120))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_Run.setFont(font)
        self.pushButton_Run.setObjectName("pushButton_Run")
        self.pushButton_Run.clicked.connect(self.run_algorithms)
        self.pushButton_Run.setIcon(QtGui.QIcon('images/carla.jpg'))
        self.pushButton_Run.setIconSize(QtCore.QSize(160, 120))

        self.label_start = QtWidgets.QLabel(Algorytmy)
        self.label_start.setGeometry(QtCore.QRect(10, 10, 200, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_start.setFont(font)
        self.label_start.setObjectName("label_start")
        self.label_koniec = QtWidgets.QLabel(Algorytmy)
        self.label_koniec.setGeometry(QtCore.QRect(10, 80, 200, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_koniec.setFont(font)
        self.label_koniec.setObjectName("label_koniec")
        self.label_start_x = QtWidgets.QLabel(Algorytmy)
        self.label_start_x.setGeometry(QtCore.QRect(10, 45, 25, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_start_x.setFont(font)
        self.label_start_x.setObjectName("label_start_x")
        self.label_start_y = QtWidgets.QLabel(Algorytmy)
        self.label_start_y.setGeometry(QtCore.QRect(90, 45, 25, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_start_y.setFont(font)
        self.label_start_y.setObjectName("label_start_y")
        self.label_koniec_x = QtWidgets.QLabel(Algorytmy)
        self.label_koniec_x.setGeometry(QtCore.QRect(10, 115, 25, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_koniec_x.setFont(font)
        self.label_koniec_x.setObjectName("label_koniec_x")
        self.label_koniec_y = QtWidgets.QLabel(Algorytmy)
        self.label_koniec_y.setGeometry(QtCore.QRect(90, 115, 25, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_koniec_y.setFont(font)
        self.label_koniec_y.setObjectName("label_koniec_y")
        self.label_algorytmy = QtWidgets.QLabel(Algorytmy)
        self.label_algorytmy.setGeometry(QtCore.QRect(10, 150, 200, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_algorytmy.setFont(font)
        self.label_algorytmy.setObjectName("label_algorytmy")
        self.label_dystans = QtWidgets.QLabel(Algorytmy)
        self.label_dystans.setGeometry(QtCore.QRect(365, 150, 200, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_dystans.setFont(font)
        self.label_dystans.setObjectName("label_start")

        self.textEdit_start_x = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_start_x.setGeometry(QtCore.QRect(30, 40, 51, 30))
        self.textEdit_start_x.setObjectName("textEdit_start_x")
        self.textEdit_start_x.setText("-50")
        self.textEdit_koniec_x = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_koniec_x.setGeometry(QtCore.QRect(30, 110, 51, 30))
        self.textEdit_koniec_x.setObjectName("textEdit_koniec_x")
        self.textEdit_koniec_x.setText("200")
        self.textEdit_koniec_y = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_koniec_y.setGeometry(QtCore.QRect(110, 110, 51, 30))
        self.textEdit_koniec_y.setObjectName("textEdit_koniec_y")
        self.textEdit_koniec_y.setText("-3")
        self.textEdit_start_y = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_start_y.setGeometry(QtCore.QRect(110, 40, 51, 30))
        self.textEdit_start_y.setObjectName("textEdit_start_y")
        self.textEdit_start_y.setText("-130")

        self.textEdit_distance_astar = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_distance_astar.setGeometry(QtCore.QRect(365, 180, 150, 30))
        self.textEdit_distance_astar.setObjectName("textEdit_distance_astar")
        self.textEdit_distance_astar.setReadOnly(True)
        self.textEdit_distance_dijkstra = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_distance_dijkstra.setGeometry(QtCore.QRect(365, 210, 150, 30))
        self.textEdit_distance_dijkstra.setObjectName("textEdit_distance_dijkstra")
        self.textEdit_distance_dijkstra.setReadOnly(True)
        self.textEdit_distance_euclidean = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_distance_euclidean.setGeometry(QtCore.QRect(365, 240, 150, 30))
        self.textEdit_distance_euclidean.setObjectName("textEdit_distance_euclidean")
        self.textEdit_distance_euclidean.setReadOnly(True)
        self.textEdit_distance_least_turns = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_distance_least_turns.setGeometry(QtCore.QRect(365, 270, 150, 30))
        self.textEdit_distance_least_turns.setObjectName("textEdit_distance_least_turns")
        self.textEdit_distance_least_turns.setReadOnly(True)

        self.checkBox_astar = QtWidgets.QCheckBox(Algorytmy)
        self.checkBox_astar.setGeometry(QtCore.QRect(10, 180, 300, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.checkBox_astar.setFont(font)
        self.checkBox_astar.setObjectName("checkBox_astar")
        self.checkBox_dijkstra = QtWidgets.QCheckBox(Algorytmy)
        self.checkBox_dijkstra.setGeometry(QtCore.QRect(10, 210, 300, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.checkBox_dijkstra.setFont(font)
        self.checkBox_dijkstra.setObjectName("checkBox_dijkstra")
        self.checkBox_euclidean = QtWidgets.QCheckBox(Algorytmy)
        self.checkBox_euclidean.setGeometry(QtCore.QRect(10, 240, 300, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.checkBox_euclidean.setFont(font)
        self.checkBox_euclidean.setObjectName("checkBox_euclidean")
        self.checkBox_least_turns = QtWidgets.QCheckBox(Algorytmy)
        self.checkBox_least_turns.setGeometry(QtCore.QRect(10, 270, 330, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.checkBox_least_turns.setFont(font)
        self.checkBox_least_turns.setObjectName("checkBox_least_turns")

        self.label_waypoinst_resolution = QtWidgets.QLabel(Algorytmy)
        self.label_waypoinst_resolution.setGeometry(QtCore.QRect(10, 310, 330, 25))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.label_waypoinst_resolution.setFont(font)
        self.label_waypoinst_resolution.setObjectName("map_resolution")

        self.textEdit_map_resoltion = QtWidgets.QTextEdit(Algorytmy)
        self.textEdit_map_resoltion.setGeometry(QtCore.QRect(160, 310, 51, 30))
        self.textEdit_map_resoltion.setObjectName("textEdit_map_resolution")
        self.textEdit_map_resoltion.setText("5")

        self.retranslateUi(Algorytmy)
        QtCore.QMetaObject.connectSlotsByName(Algorytmy)

    def run_algorithms(self):
        waypoints_map_range = int(self.textEdit_map_resoltion.toPlainText())
        self.textEdit_distance_least_turns.setText("")
        self.textEdit_distance_astar.setText("")
        self.textEdit_distance_dijkstra.setText("")
        self.textEdit_distance_euclidean.setText("")

        print("Connecting")
        client = carla.Client('localhost', 2000)
        client.set_timeout(10.0)

        world = client.get_world()
        map_ = world.get_map()
        tmp = int(self.textEdit_map_resoltion.toPlainText())
        all_waypoints = map_.generate_waypoints(waypoints_map_range)

        start_point = self.find_closest_waypoint_by_xy(all_waypoints, x=int(self.textEdit_start_x.toPlainText()),
                                                       y=int(self.textEdit_start_y.toPlainText()))
        end_point = self.find_closest_waypoint_by_xy(all_waypoints, x=int(self.textEdit_koniec_x.toPlainText()),
                                                     y=int(self.textEdit_koniec_y.toPlainText()))

        print("Processing route")
        guide = Guide(start_point, end_point)

        computed_routes = {}
        if self.checkBox_least_turns.isChecked():
            start_time = time.time()
            route = getattr(guide, "guide_most_forward_route")(all_waypoints=all_waypoints,
                                                               waypoints_range=waypoints_map_range,
                                                               finish_approximation=finish_distance_approximation)
            end_time = time.time() - start_time
            total_distance = 0
            for i in range(len(route) - 1):
                total_distance += self.heuristic_distance(route[i + 1], route[i])
            computed_routes["guide_most_forward_route"] = {"route": route, "time": end_time, "distance": total_distance,
                                                           "color": "r--", "marker_size": 10}
            self.textEdit_distance_least_turns.setText(str(round(total_distance, 3)))
        if self.checkBox_astar.isChecked():
            start_time = time.time()
            route = getattr(guide, "guide_a_star")(waypoints_range=waypoints_map_range, all_waypoints=all_waypoints)
            end_time = time.time() - start_time
            total_distance = 0
            for i in range(len(route) - 1):
                total_distance += self.heuristic_distance(route[i + 1], route[i])
            computed_routes["guide_a_star"] = {"route": route, "time": end_time, "distance": total_distance,
                                               "color": "g-.", "marker_size": 10}
            self.textEdit_distance_astar.setText(str(round(total_distance, 3)))

        if self.checkBox_dijkstra.isChecked():
            start_time = time.time()
            route = getattr(guide, "guide_dijkstra")(waypoints_range=waypoints_map_range,
                                                     all_waypoints=all_waypoints)
            end_time = time.time() - start_time
            total_distance = 0
            for i in range(len(route) - 1):
                total_distance += self.heuristic_distance(route[i + 1], route[i])
            computed_routes["guide_dijkstra"] = {"route": route, "time": end_time, "distance": total_distance,
                                                 "color": "mx", "marker_size": 5}
            self.textEdit_distance_dijkstra.setText(str(round(total_distance, 3)))

        if self.checkBox_euclidean.isChecked():
            start_time = time.time()
            route = getattr(guide, "guide_shortest_euclidean_distance")(waypoints_range=waypoints_map_range,
                                                                        all_waypoints=all_waypoints)
            end_time = time.time() - start_time
            total_distance = 0
            for i in range(len(route) - 1):
                total_distance += self.heuristic_distance(route[i + 1], route[i])
            computed_routes["guide_shortest_euclidean_distance"] = {"route": route, "time": end_time,
                                                                    "distance": total_distance,
                                                                    "color": "k^", "marker_size": 3}
            self.textEdit_distance_euclidean.setText(str(round(total_distance, 3)))

        # self.textEdit_distance_least_turns.setText(str(round(total_distance)))
        # self.textEdit_distance_least_turns.repaint()
        print("Plotting")
        for way_point in all_waypoints:
            plt.scatter(way_point.transform.location.x, way_point.transform.location.y, s=0.2, c="b")
        plt.scatter(start_point.transform.location.x, start_point.transform.location.y, s=[50], c="c", label="start")
        plt.scatter(end_point.transform.location.x, end_point.transform.location.y, s=[50], c="m", label="end")

        x, y = [], []
        for algorithm, data in computed_routes.items():
            x = [way_point.transform.location.x for way_point in data["route"]]
            y = [way_point.transform.location.y for way_point in data["route"]]
            plt.plot(x, y, data["color"], label=algorithm, markersize=data["marker_size"])
        plt.legend()
        plt.show()
        print("Finished")

    def find_closest_waypoint_by_xy(self, list_of_waypoints, x, y):
        best_dist = math.inf
        best_waypoint = list_of_waypoints[0]
        for waypoint in list_of_waypoints:
            x_distance = waypoint.transform.location.x - x
            y_distance = waypoint.transform.location.y - y
            dist = math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))
            if dist < best_dist:
                best_dist = dist
                best_waypoint = waypoint
        return best_waypoint

    def heuristic_distance(self, waypoint1, waypoint2):
        # Distance between two waypoints
        x_distance = waypoint1.transform.location.x - waypoint2.transform.location.x
        y_distance = waypoint1.transform.location.y - waypoint2.transform.location.y
        return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))

    def retranslateUi(self, Algorytmy):
        _translate = QtCore.QCoreApplication.translate
        Algorytmy.setWindowTitle(_translate("Algorytmy", "Algorytmy"))
        self.pushButton_Run.setText(_translate("Algorytmy", ""))
        self.label_start.setText(_translate("Algorytmy", "Punkt startowy"))
        self.label_koniec.setText(_translate("Algorytmy", "Punkt końcowy"))
        self.label_start_x.setText(_translate("Algorytmy", "x:"))
        self.label_start_y.setText(_translate("Algorytmy", "y:"))
        self.label_koniec_x.setText(_translate("Algorytmy", "x:"))
        self.label_koniec_y.setText(_translate("Algorytmy", "y:"))
        self.label_algorytmy.setText(_translate("Algorytmy", "Wybór algorytmów:"))
        self.label_dystans.setText(_translate("Algorytmy", "Dystanse:"))
        self.label_waypoinst_resolution.setText(_translate("Algorytmy", "Rozdzielczość mapy:"))
        self.checkBox_astar.setText(_translate("Algorytmy", "Algorytm A*"))
        self.checkBox_dijkstra.setText(_translate("Algorytmy", "Algorytm Dijkstry"))
        self.checkBox_euclidean.setText(_translate("Algorytmy", "Norma euklidesowa"))
        self.checkBox_least_turns.setText(_translate("Algorytmy", "Najdłuższy prosty odcinek"))
        self.checkBox_least_turns.setText(_translate("Algorytmy", "Najdłuższy prosty odcinek"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Algorytmy = QtWidgets.QDialog()
    ui = Ui_Algorytmy()
    ui.setupUi(Algorytmy)
    Algorytmy.show()
    sys.exit(app.exec_())
