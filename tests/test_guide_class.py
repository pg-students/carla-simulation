"""
Env:
Windows 7
Carla 0.9.5
Unreal Engine 4.22.3
Python 3.7.4
"""
import carla

from src import consts
from src.navigation.automatic_control import navigation_loop
from src.tools.json_parser import ConfigParser


def main():
    client = carla.Client('localhost', 2000)
    client.set_timeout(10.0)

    config_parser = ConfigParser(consts.TEST_CONFIG_DIR)

    world = client.get_world()
    map_ = world.get_map()
    blueprint_library = world.get_blueprint_library()
    spawn_points_list = map_.get_spawn_points()

    car_bp = blueprint_library.filter("vehicle.bmw.*")[0]
    car_bp2 = blueprint_library.filter("vehicle.bmw.*")[1]

    # Insert spawn points here
    spawn1 = spawn_points_list[21]
    spawn2 = spawn_points_list[0]

    car_actor = world.spawn_actor(car_bp, spawn1)
    car_actor2 = world.spawn_actor(car_bp2, spawn2)

    navigation_loop(client=client, config_dict=config_parser.config, actor=car_actor, destination=spawn2)

    car_actor.destroy()
    car_actor2.destroy()


if __name__ == "__main__":
    main()
